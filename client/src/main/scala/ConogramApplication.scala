import account.Account
import account.controller.AccountController
import account.model.service.{BrokerREST, TokenManagerJWT}
import account.view.{AccountGUI, AccountViewObserver}
import game.multiplayer.controller.ChallengeController
import game.multiplayer.view.{MultiplayerMenuGUI, MultiplayerMenuView}


/**
  * Main of the system, deals with setting the view for the login and
  * the view for the choice of parameters with which to start a match.
  * It also sets the related controllers.
  */
object ConogramApplication extends App {

  private val ServerURI = "http://localhost:9000"

  private val accountView: AccountGUI = AccountGUI()

  private val accountController: AccountController[String] =
    AccountController(accountView, BrokerREST(ServerURI), TokenManagerJWT.withHash())

  private val accountViewObserver = new AccountViewObserver {
    def loginEvent(username: String, password: String): Unit =
      accountController.login(username, password)

    def signupEvent(username: String, password: String): Unit =
      accountController.signup(username, password)
  }

  accountView.addAccountViewObserver(accountViewObserver)

  accountController.addLoginObserver({ user =>
    Account.currentUser = Some(user)
    setComponentsForChallenge()
  })

  accountView.display()

  private def setComponentsForChallenge(): Unit = {
    val multiPlayerMenuView: MultiplayerMenuView = MultiplayerMenuGUI()
    val challengeController = ChallengeController(multiPlayerMenuView)

    multiPlayerMenuView
      .addORandomChallengeObserver(
        challengeController.challengeRandomPlayer)

    multiPlayerMenuView
      .addUserChallengeObserver(
        challengeController.challengePlayer)

    multiPlayerMenuView.display()
  }

}
