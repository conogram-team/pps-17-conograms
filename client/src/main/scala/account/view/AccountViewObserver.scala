package account.view

/** A subscriber for [[AccountView]] events */
trait AccountViewObserver {
  /** Called when a user assert login intention through UI */
  def loginEvent(username: String, password: String): Unit
  /** Called when a user assert signup intention through UI */
  def signupEvent(username: String, password: String): Unit
}
