package account.view

import game.singleplayer.view.View

/** The account UI
  *  provide display functions to a controller
  */
trait AccountView extends View {
  /** Inform the user that an error occurs
    *
    *  @param error the information error to be displayed
    */
  def informError(error: String): Unit
  /** Add a observer which will be notified when some action where executed by the user */
  def addAccountViewObserver(observer: AccountViewObserver): Unit
}
