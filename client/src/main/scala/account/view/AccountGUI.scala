package account.view

import scala.language.implicitConversions
import javax.swing._
import utility.swing.SwingHelper._

/** Frame for login and signup with the implementation of [[AccountView]] trait as expected
  *  login button that call [[AccountViewObserver.loginEvent]] of subscribers
  *  signup button that call [[AccountViewObserver.signupEvent]] of subscribers
  *  text fields for parameters
  */
class AccountGUI extends JFrame with AccountView {
  val preferredWidth = 200
  val preferredHeight = 230
  val loginButtonText = " Log In "
  val signupButtonText = "Sing Up"
  val usernameLabelText = "Username:"
  val passwordLabelText = "Password:"

  val usernameLabel: JLabel = new JLabel(usernameLabelText)
  val usernameField: JTextField = new JTextField()
  val passwordLabel: JLabel = new JLabel(passwordLabelText)
  val passwordField: JPasswordField = new JPasswordField()
  val loginButton: JButton = new JButton(loginButtonText)
  val signupButton: JButton = new JButton(signupButtonText)
  val inform: JLabel = new JLabel()

  private var observers: Seq[AccountViewObserver] = Seq()

  this.setLayout(new BoxLayout(this.getContentPane, BoxLayout.Y_AXIS))
  this.addAllComponent(usernameLabel, usernameField, passwordLabel, passwordField, loginButton, signupButton, inform)

  usernameField.setMaximumSize(Integer.MAX_VALUE, usernameField.getPreferredSize.height)
  passwordField.setMaximumSize(Integer.MAX_VALUE, passwordField.getPreferredSize.height)
  this.setPreferredSize(preferredWidth, preferredHeight)
  this.pack()
  //noinspection ScalaStyle
  this.setLocationRelativeTo(null)

  loginButton.addActionListener({
    cleanView()
    observers.foreach(_.loginEvent(usernameField.getText, passwordField.getPassword))
  })

  signupButton.addActionListener({
    cleanView()
    observers.foreach(_.signupEvent(usernameField.getText, passwordField.getPassword))
  })

  override def addAccountViewObserver(observer: AccountViewObserver): Unit = {
    observers = observers :+ observer
  }

  override def display(): Unit = safeInvokeAndWait{ this.setVisible(true) }

  override def close(): Unit = safeInvokeAndWait{ this.dispose() }

  override def informError(error: String): Unit = safeInvokeAndWait{ inform.setText(error) }

  private implicit def arrayCharToString(chars: Array[Char]): String = chars.mkString

  private def cleanView(): Unit = {
    inform.setText("")
  }
}

/** Factory for [[AccountGUI]] */
object AccountGUI {
  /** Produces an [[AccountGUI]]: Frame for login and signup for more information see: [[AccountGUI]] class */
  def apply(): AccountGUI = new AccountGUI()
}
