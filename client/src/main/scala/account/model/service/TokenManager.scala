package account.model.service

import account.model.service.support.Cryptography.defaultUserApiEncode

/** An object who knows how to process a token
  *
  *  @tparam Token whatever a token is, we know that,
  *               it can be built from a username and password
  *               it can be converted into a string to be sent to a server for authentication purposes
  */
trait TokenManager[Token] {
  /**  Compute a token starting from a pair consisting of a username and a password */
  def buildToken(username: String, password: String): Token
  /**  Takes a token and compute a string which can be used for authenticate to a server */
  def tokenToString(token: Token): String
}

/** Mixin for [[TokenManager]]
  *  this add an hash operation on the password before build a token
  */
trait TokenManagerWithHash[Token] extends TokenManager[Token] {
  abstract override def buildToken(username: String, password: String): Token =
    super.buildToken(username, defaultUserApiEncode(password))
}
