package account.model.service

import pdi.jwt.{Jwt, JwtAlgorithm}

/** A [[TokenManager]] that process string token with the Json Web Token encoding */
trait TokenManagerJWTImplementation extends TokenManager[String] {
  /** The algorithm to be used to encode the token */
  def jwtAlgorithm: JwtAlgorithm

  override def buildToken(username: String, password: String): String =
    Jwt.encode(s"""{"user":$username}""", password, jwtAlgorithm)

  override def tokenToString(token: String): String = token
}

private class TokenManagerJWT(
    override val jwtAlgorithm: JwtAlgorithm)
  extends TokenManagerJWTImplementation

private class TokenManagerJWTWithHash(jwtAlgorithm: JwtAlgorithm)
  extends TokenManagerJWT(jwtAlgorithm) with TokenManagerWithHash[String]

/** Factory for [[TokenManagerJWT]] */
object TokenManagerJWT {
  /**  Produce a [[TokenManager]]that process string token with the Json Web Token encoding
    *
    * @param jwtAlgorithm the algorithm that carry out the encoding, default HS256
    */
  def apply(jwtAlgorithm: JwtAlgorithm = JwtAlgorithm.HS256): TokenManagerJWTImplementation =
    new TokenManagerJWT(jwtAlgorithm)
  /**  Produce a [[TokenManager]] that process string token with the Json Web Token encoding
    *  The password is hashed before produce the token
    * @param jwtAlgorithm the algorithm that carry out the encoding, default HS256
    */
  def withHash(jwtAlgorithm: JwtAlgorithm = JwtAlgorithm.HS256): TokenManagerJWTImplementation =
    new TokenManagerJWTWithHash(jwtAlgorithm)
}
