package account.model.service

import scala.concurrent.Future

/** A mediator between client and server
  *
  *  it take cares of connection and protocols for perform request to the server
  */
trait AccountServiceBroker {
  /** Send a signup request to the server, asynchronous method
    *
    *  @param username the username to be registered
    *  @param password the login password
    *  @return a tuple, as a Future,
    *          composed by a boolean that represent the success of the request and an informative message
    *
    *          true if the request was successful i.e. account registered
    *          false if the server has rejected the request e.g. invalid length or username already present
    *          the Future fails e.g if a connection error is detected
    *          the string contains the motivation of rejection or a success informative message
    */
  def signup(username: String, password: String): Future[(Boolean, String)]
  /** Send a login request to the server, asynchronous method
    *
    *  @param username the username that corresponds to the user indicated into the token
    *  @param token the token needed to authenticate the user
    *  @return a tuple, as a Future,
    *          composed by a boolean that represent the success of the request and an informative message
    *
    *          true if the request was successful i.e. the token is valid for future requests (until expiration)
    *          false if the server has rejected the request e.g. invalid token or username
    *          the Future fails e.g. if a connection error is detected
    *          the string contains the motivation of rejection or a success informative message
    */
  def login(username: String, token: String): Future[(Boolean, String)]
}
