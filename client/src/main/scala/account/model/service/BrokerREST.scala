package account.model.service

import rest.user.api.UserApi
import rest.user.model.User
import account.model.service.support.Cryptography._
import account.model.service.support.Network._

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

/** The implementation of [[AccountServiceBroker]] that is conform to the REST API provided by the server
  *  it needs the definition of UserApi
  *  and it can be mixed in with a trait for the conversion of the http response to an appropriate message
  *  known traits: [[responseToUIMessage]]
  */
trait BrokerREST extends AccountServiceBroker {

  private val authenticationHeader = "Authorization"

  /** The API provided by the server */
  def userApi: UserApi

  override def signup(username: String, password: String): Future[(Boolean, String)] = {
    val user = User(username = username, passwordHash = Some(defaultUserApiEncode(password)))
    processRequestFuture(userApi.createUserAsync(user))
  }

  override def login(username: String, token: String): Future[(Boolean, String)] = {
    bindRemoteSystem(defaultAddress, 0)
    userApi.addHeader(authenticationHeader, token)
    val user = User(username = username, ip = userBoundCompleteAddress)
    processRequestFuture(userApi.loginUserAsync(user))
  }

  private def processRequestFuture(request: Future[String]): Future[(Boolean, String)] = {
    val responsePromise = Promise[(Boolean, String)]()
    val responseFuture = responsePromise.future

    request onComplete {
      case Success("") => responsePromise success (true, "")
      case Success(message) => responsePromise success (false, getResultMessage(message))
      case Failure(error) => responsePromise failure error
    }

    responseFuture
  }

  protected def getResultMessage(response: String): String = response
}

/** Define a procedure which elaborates the server response to get an appropriate message to be shown to the user
  *  to be used with [[BrokerREST]]
  */
trait responseToUIMessage extends BrokerREST {

  protected val generalError = "Some error occurred"

  protected override def getResultMessage(response: String): String = {
    var responseMessage = generalError
    if(response.contains("BadRequest")) {
      val detailSearch = "detail\":\""
      val messageBegin = response.indexOf(detailSearch) + detailSearch.length
      val messageEnd = response.lastIndexOf("\"")
      if(messageBegin > 0 && messageBegin < messageEnd && messageEnd < response.length) {
        responseMessage = response.substring(messageBegin, messageEnd)
      }
    }
    responseMessage
  }
}

private class BrokerRESTWithPrettyResponse(val userApi: UserApi) extends BrokerREST with responseToUIMessage

/** Factory for [[BrokerREST]]  */
object BrokerREST {
  /** Creates a [[BrokerREST]] with the given UserApi, it has already the support for "pretty responses"
    *  "pretty responses": the methods return message ready to be shown to the user
    *
    * @param userApi the API provided by the server
    */
  def apply(userApi: UserApi): BrokerREST = new BrokerRESTWithPrettyResponse(userApi)
  /** Creates a [[BrokerREST]], it has already the support for "pretty responses"
    *  "pretty responses": the methods return message ready to be shown to the user
    *
    * @param serverURI the uri of the server
    */
  def apply(serverURI: String): BrokerREST = new BrokerRESTWithPrettyResponse(new UserApi(serverURI))
}
