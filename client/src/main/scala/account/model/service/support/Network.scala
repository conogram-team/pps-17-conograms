package account.model.service.support

import java.net.{InetAddress, NetworkInterface}

import akka.actor._
import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._

/** facility for client networking */
object Network {
  /** the name of the application actor system */
  val SystemName = "PlayerSystem"
  /** get the address of this client as agreed in the protocol */
  def defaultAddress: String = localAddress
  /** get the local address of the machine */
  def localAddress: String =
    NetworkInterface.getNetworkInterfaces
      .map(_.getInetAddresses)
      .filter(_.hasMoreElements)
      .map(_.nextElement().getHostAddress)
      .filter(isLanIP).toSeq
      .headOption.getOrElse(InetAddress.getLocalHost.getHostAddress)

  /** Check if an address came from a lan or wan
    *  Note that it does't verify that the ip is correctly formatted
    */
  def isLanIP(ip: String): Boolean =
    ip.startsWith("192.168") ||
    ip.startsWith("10.")     ||
    ip.startsWith("172.16")  ||
    ip.startsWith("172.17")  ||
    ip.startsWith("172.18")  ||
    ip.startsWith("172.19")  ||
    ip.startsWith("172.2")   ||
    ip.startsWith("172.30")  ||
    ip.startsWith("172.31")

  private var actorSystem: Option[ActorSystem] = None

  /** Binds the actor system to an address and a port */
  def bindRemoteSystem(address: String, port: Int): Unit = {
    if(actorSystem.isEmpty) {
      val configurationWithAddressAndPort =
        ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + address)
          .withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port))
          .withFallback(ConfigFactory.load())
      actorSystem = Some(ActorSystem.create(SystemName, configurationWithAddressAndPort))
    }
  }

  /** The actor system used in this application, instantiated with bindRemoteSystem */
  def applicationActorSystem: Option[ActorSystem] = actorSystem

  /** Get the complete address that is used in the actor system if bindRemoteSystem was called
    *  it contains address+port
    */
  def userBoundCompleteAddress: Option[String] = (userBoundAddress, userBoundPort) match {
      case (Some(ip), Some(port)) => Some(ip + ":" + port)
      case _ => None
  }

  import utility.actor.ActorSystemExtension._

  /** Get the port that is used in the actor system if bindRemoteSystem was called */
  def userBoundPort: Option[Int] = actorSystem match {
    case Some(system) =>  AddressExtension.portOf(system)
    case _ => None
  }

  /** Get the address that is used in the actor system if bindRemoteSystem was called */
  def userBoundAddress: Option[String] = actorSystem match {
    case Some(system) =>  AddressExtension.hostOf(system)
    case _ => None
  }
}
