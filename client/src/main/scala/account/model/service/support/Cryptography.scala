package account.model.service.support

/**
  * Get from https://stackoverflow.com/questions/46329956/how-to-correctly-generate-sha-256-checksum-for-a-string-in-scala
  */

import java.security.MessageDigest

/** Utilities for cryptography and hash functions */
object Cryptography {
  /** the default encoding agreed in server protocol */
  def defaultUserApiEncode(message: String): String = encodeSHA256(message)

  /** Returns the hash of the specified message, produced with SHA-256 */
  def encodeSHA256(message: String): String =
    MessageDigest.getInstance("SHA-256")
    .digest(message.getBytes("UTF-8"))
    .map("%02x".format(_)).mkString
}
