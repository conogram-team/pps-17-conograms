package account.model

/** A person who uses our application.
  *
  *  @tparam Token the key that enable authentication in the server
  */
trait ConogramUser[Token] {
  /** Username getter.
    *
    *  the username identifies the user
    */
  def username: String

  /** Token getter, as option because it may not be ready or eventually removed.
    *
    *  token is the key that enable authentication in the server
    */
  def token: Option[Token]
  /** Creates a copy of user with the new token
    *
    *  token is the key that enable authentication in the server
    */
  def withToken(token: Option[Token]): ConogramUser[Token]

  /** Location getter, as option because it may not be set or eventually removed.
    *
    *  An IP or network address
    */
  def location: Option[String]
  /** Creates a copy of user with the new location
    *
    *  An IP or network address
    */
  def withLocation(location: Option[String]): ConogramUser[Token]
}

private case class BaseConogramUser[Token](
    override val username: String,
    override val token: Option[Token],
    override val location: Option[String])
  extends ConogramUser[Token] {
  override def withToken(token: Option[Token]): ConogramUser[Token] = copy(token = token)
  override def withLocation(location: Option[String]): ConogramUser[Token] = copy(location = location)
}

/** Factory for [[account.model.ConogramUser]] instances. */
object ConogramUser {
  /** Creates a user with username and eventually a token.
    *
    *  @param username their username
    *  @param token the token needed to authenticate on the server, default:
    *  @param location the user ip or network address
    */
  def apply[Token](username: String, token: Option[Token] = None, location: Option[String] = None): ConogramUser[Token]
    = BaseConogramUser(username, token, location)
}
