package account

import account.model.ConogramUser

/** A singleton for track the logged user (thread-safe) */
object Account {

  private var _currentUser: Option[ConogramUser[String]] = None

  /** Get the application user if he logs in */
  def currentUser: Option[ConogramUser[String]] = synchronized { _currentUser }

  /** Set the application user, use only after login for share the user reference
    *  set None for clear the reference
    */
  def currentUser_=(currentUser: Option[ConogramUser[String]]): Unit = synchronized { _currentUser = currentUser }
}
