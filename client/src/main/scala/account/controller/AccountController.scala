package account.controller

import account.model.service.support.Network._
import account.model.service.{AccountServiceBroker, TokenManager}
import account.model.ConogramUser
import account.view.AccountView

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}
import scala.util.{Failure, Success}

/** The controller for account section
  *  it coordinates an [[account.view.AccountView]] and a [[account.model.service.AccountServiceBroker]] to produce a User
  *
  *  @tparam Token The type of token that the User and the [[account.model.service.TokenManager]] will use
  */
trait AccountController[Token] {
  /** The UI that will be updated */
  def accountView: AccountView
  /** The mediator that will be used to interact with the server */
  def accountServiceBroker: AccountServiceBroker
  /** The object who know how to process the Token used for server interaction */
  def tokenManager: TokenManager[Token]

  /** The signup procedure with appropriate updates of view and model */
  def signup(username: String, password: String): Unit
  /** The login procedure with appropriate updates of view and model */
  def login(username: String, password: String): Unit

  /** Register an observer that consume the produced user after successful login procedure */
  def addLoginObserver(observer: ConogramUser[Token] => Unit): Unit
}

/** Implement [[AccountController]], both login and signup methods
  *  - will notify the observers with produced [[account.model.ConogramUser]] in case of success and then close the view
  *  - will inform the view in case of errors
  */
trait AccountControllerImplementation[Token] extends AccountController[Token] {

  private implicit val futureContext: ExecutionContextExecutor = ExecutionContext.global

  protected val failureMessage = "Please check your connection"
  protected val emptyFieldMessage = "Please provide a username and a password"

  private var observers: Seq[ConogramUser[Token] => Unit] = Seq()

  override def addLoginObserver(observer: ConogramUser[Token] => Unit): Unit = {
    observers = observers :+ observer
  }

  override def signup(username: String, password: String): Unit = {
    if (checkFields(username, password)) {
      accountServiceBroker.signup(username, password) onComplete {
        case Success((true, _)) => login(username, password)
        case Success((false, message)) => accountView.informError(message)
        case Failure(_) => accountView.informError(failureMessage)
      }
    }
  }

  override def login(username: String, password: String): Unit = {
    if (checkFields(username, password)) {
      val token = tokenManager.buildToken(username, password)

      accountServiceBroker.login(username, tokenManager.tokenToString(token)) onComplete {
        case Success((true, _)) =>
          loginSuccessUpdateModel(token)
          loginSuccessUpdateView()
        case Success((false, message)) => accountView.informError(message)
        case Failure(_) => accountView.informError(failureMessage)
      }

    }

    def loginSuccessUpdateModel(token: Token): Unit = {
      val user = ConogramUser(username, Some(token), userBoundCompleteAddress)
      observers.foreach(_ (user))
    }

    def loginSuccessUpdateView(): Unit = {
      accountView.close()
    }
  }

  protected def checkFields(username: String, password: String): Boolean = {
    (username, password) match {
      case ("", _) | (_, "") => notifyEmptyField();false
      case (_, _) => true
    }
  }

  protected def notifyEmptyField(): Unit = {
    accountView.informError(emptyFieldMessage)
  }
}

private class GeneralAccountController[Token](
    override val accountView: AccountView,
    override val accountServiceBroker: AccountServiceBroker,
    override val tokenManager: TokenManager[Token])
  extends AccountControllerImplementation[Token]

/** Factory for [[AccountController]] instances. */
object AccountController {
  /** Produces an [[AccountController]] that coordinates the specified
    * [[account.view.AccountView]] and [[account.model.service.AccountServiceBroker]]
    *  For more information see [[AccountController]] trait
    */
  def apply[Token](
      accountView: AccountView,
      accountServiceBroker: AccountServiceBroker,
      tokenManager: TokenManager[Token]
    ): AccountController[Token] = new GeneralAccountController(accountView, accountServiceBroker, tokenManager)
}
