package game.multiplayer.actor

import java.sql.Timestamp

import account.model.service.support.Network
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSelection, Props}
import game.multiplayer.controller.MultiplayerMatchController
import game.singleplayer.model.Player

import scala.collection.mutable

case class CellSelectedFromChallegePlayer(point: (Int, Int), timestamp: Timestamp)

case class Consensus(point: (Int, Int))

case class NotConsensus(point: (Int, Int))

case class MessageCellSelected(point: (Int, Int), timestamp: Timestamp)

object CloseMessage

object NotifyPlayerToCloseMatch

object Match {
  def props: Props = Props[Match]
}

class Match extends Actor {
  private val clientMatchName = "MatchClient"
  private val serverMatchName = "MatchServer"
  private var pendingQueue: mutable.Map[(Int, Int), Timestamp] = mutable.Map[(Int, Int), Timestamp]()
  private var adversaryMatchActor: Option[ActorSelection] = None
  private implicit val numberOfAvailableLives: Int = 3
  var multiplayerMatchController: Option[MultiplayerMatchController[Boolean]] = None
  private val defaultStatus = true
  private val playerActor: ActorRef = context.parent
  context.actorOf(MatchClient.props, clientMatchName)
  context.actorOf(MatchServer.props, serverMatchName)

  def receive: Receive = {
    case SendToMatch(matchController: MultiplayerMatchController[Boolean], adversaryActorPath: String) =>
      adversaryMatchActor = Some(Network.applicationActorSystem.get.actorSelection(adversaryActorPath + "/MatchServer"))
      multiplayerMatchController = Some(matchController)
    case CloseMessage => playerActor ! NotifyPlayerToCloseMatch
  }

  private object MatchClient {
    def props: Props = Props(new MatchClient)
  }

  private class MatchClient extends Actor with ActorLogging {
    override def preStart(): Unit = log.info("matchClient start")

    override def postStop(): Unit = log.info("matchClient stop")

    override def receive: Receive = {
      case MessageCellSelected(point, timestamp) => notifyChallangerAndAddCellToPendingQueue(point,timestamp)
      case Consensus(point) => updateSelectionCellByPlayer(point,multiplayerMatchController.get.localPlayer.get)
      case NotConsensus(point) => updateSelectionCellByPlayer(point,multiplayerMatchController.get.getChallenger)

    }

    /**notifies the challenger that a cell has been selected and adds it to the pending queue of the selected cells
      *
      * @param point selected
      * @param timestamp when it was selected
      */
    private def notifyChallangerAndAddCellToPendingQueue(point: (Int,Int),timestamp: Timestamp) :Unit={
      pendingQueue.put(point, timestamp)
      adversaryMatchActor.get ! CellSelectedFromChallegePlayer(point, timestamp)
    }

    /** Add the selected cell from the player to the grid and removes it from the pending queue
      *
      * @param point cell that must be added
      * @param player who selected the cell
      */
    private def updateSelectionCellByPlayer(point:(Int,Int), player: Player): Unit ={
      addSelectedCellIn(point,player)
      pendingQueue.remove(point)
    }
  }

  private object MatchServer {
    def props: Props = Props(new MatchServer)
  }

  private class MatchServer extends Actor with ActorLogging {
    override def receive: Receive = {
      case CellSelectedFromChallegePlayer(point: (Int, Int), timestamp: Timestamp) =>
        if (verifyClickInTime(point, timestamp)) {
          addSelectedCellIn(point,multiplayerMatchController.get.getChallenger)
          sender() ! Consensus(point)
        } else {
          addSelectedCellIn(point, multiplayerMatchController.get.localPlayer.get)
          sender() ! NotConsensus(point)
        }
    }

    /**check if the cell is in the pendingQueue and which cell has been selected before
      *
      * @param cell that has been selected
      * @param time when it has been selected
      * @return if the cell was selected before the opponent
      */
    private def verifyClickInTime(cell: (Int, Int), time: Timestamp): Boolean =
      pendingQueue.isEmpty || pendingQueue.filterKeys(p => p == cell)(cell).after(time)
  }

  /** Add the selected cell from the player to the grid
    *
    * @param point cell that must be added
    * @param player who selected the cell
    */
  private def addSelectedCellIn(point: (Int,Int), player: Player): Unit={
    multiplayerMatchController.get.addSelectedCell(point, defaultStatus, player)
  }

}


