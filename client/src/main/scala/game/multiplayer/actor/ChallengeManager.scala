package game.multiplayer.actor

import account.Account
import account.model.ConogramUser
import akka.actor.{Actor, ActorRef, ActorSelection, Props}
import game.multiplayer.actor.ChallengeResponse.{Accept, Reject, StartMatch}
import game.multiplayer.controller.{ChallengeController, MultiplayerMatchController}
import game.singleplayer.model.Player
import game.singleplayer.model.grid.Grid
import game.singleplayer.model.grid.gridfactory.BooleanRandomGridFactory

object ChallengeManager {
  def props(implicit challengeController: ChallengeController): Props = Props(new ChallengeManager(challengeController))
}

private class ChallengeManager(private val challengeController: ChallengeController) extends Actor {
  private val localMatchActor: ActorRef = context.actorOf(Props[Match], ChildActor.LocalMatchActor.name)
  var adversaryChildrenActors: Option[(ActorSelection, ActorSelection, ActorSelection)] = None
  var gridToSend: Option[Grid[Boolean]] = None
  var adversaryIp: Option[String] = None
  context.actorOf(ChallengeManagerClient.props, ChildActor.Client.name)
  context.actorOf(ChallengeManagerServer.props, ChildActor.Server.name)

  override def receive: Receive = {
    case ChallengeRequestTo(ip, (rows, columns), fullCellsNumber) =>
      setTheGridToSendWith((rows, columns), fullCellsNumber)
      setTheAdversaryActorWith(ip)
      getAdversaryChildActor(ChildActor.Server) ! ChallengeRequest(Account.currentUser.get)
    case Reject => getAdversaryChildActor(ChildActor.Server) ! Reject
    case Accept(challengedUser) =>
      val challengerClient = getAdversaryChildActor(ChildActor.Client)
      challengerClient ! Accept(challengedUser)
    case NotifyPlayerToCloseMatch =>
      context.stop(self)
      getAdversaryChildActor(ChildActor.Server) ! NotifyPlayerToCloseMatch
  }

  private def getAdversaryActorsWith(ip: String): Option[(ActorSelection, ActorSelection, ActorSelection)] =
    Some(
      context.actorSelection(ActorPathBuilder.getActorPathOf(ChildActor.Client, Some(ip), Some(Seq(ChildActor.Client.name)))),
      context.actorSelection(ActorPathBuilder.getActorPathOf(ChildActor.Server, Some(ip), Some(Seq(ChildActor.Server.name)))),
      context.actorSelection(ActorPathBuilder.getActorPathOf(ChildActor.LocalMatchActor, Some(ip), Some(Seq(ChildActor.LocalMatchActor.name))))
    )

  private def getAdversaryChildActor(peer: ChildActor.Value): ActorSelection =
    peer match {
      case ChildActor.Client => adversaryChildrenActors.get._1
      case ChildActor.Server => adversaryChildrenActors.get._2
    }

  private def notifyViewChallenge(challengeResponse: ChallengeResponse.EnumVal): Unit = challengeResponse match {
    case Accept(user) =>
      challengeController.notifyViewOf(Accept(user))
    case Reject => challengeController.notifyViewOf(Reject)
    case StartMatch(matchController, user) =>
      challengeController.notifyViewOf(StartMatch(matchController, user))
  }

  private def getGridToSend(size: (Int, Int), numberOfFullCells: Int): Option[Grid[Boolean]] =
    Some(BooleanRandomGridFactory(size._1, size._2, numberOfFullCells))

  private def setTheGridToSendWith(gridSize: (Int, Int), fullCellsNumber: Int): Unit = {
    gridToSend = getGridToSend((gridSize._1, gridSize._2), fullCellsNumber)
  }

  private def setTheAdversaryActorWith(ip: String): Unit = {
    adversaryChildrenActors = getAdversaryActorsWith(ip)
    adversaryIp = Some(ip)
  }

  private case object ChallengeManagerClient {
    def props: Props = Props(new ChallengeManagerClient)
  }

  private case class ChallengeManagerClient() extends Actor {
    override def receive: Receive = {
      case Accept(challengedUser) =>
        gridToSend match {
          case Some(grid) =>
            val challengerMatchController = MultiplayerMatchController(grid)
            val challengedMatchController = MultiplayerMatchController(grid)
            val challengerPlayer = Player(Account.currentUser.get)
            val challengedPlayer = Player(challengedUser)
            getAdversaryChildActor(ChildActor.Client) ! StartMatch(challengedMatchController, Account.currentUser.get)

            setMatchControllerFor((challengerPlayer, challengedPlayer), challengerMatchController)

            localMatchActor ! SendToMatch(challengerMatchController, ActorPathBuilder.getActorPathOf(ChildActor.LocalMatchActor, adversaryIp))

            notifyViewChallenge(StartMatch(challengerMatchController, challengedUser))
          case None => self ! Reject
        }
      case StartMatch(matchController, challengerUser) =>
        val challengerPlayer = Player(Account.currentUser.get)
        val challengedPlayer = Player(challengerUser)

        setMatchControllerFor((challengerPlayer, challengedPlayer), matchController)

        localMatchActor ! SendToMatch(matchController, ActorPathBuilder.getActorPathOf(ChildActor.LocalMatchActor, adversaryIp))
        notifyViewChallenge(StartMatch(matchController, challengerUser))
    }
  }

  private def setMatchControllerFor(playersInMatch: (Player, Player), matchController: MultiplayerMatchController[Boolean]): Unit = {
    matchController.localPlayer_=(playersInMatch._1)
    matchController.addPlayerInMatch(playersInMatch._2)
  }

  private case object ChallengeManagerServer {
    def props: Props = Props(new ChallengeManagerServer)
  }

  private case class ChallengeManagerServer() extends Actor {
    override def receive: Receive = {
      case ChallengeRequest(challengerUser: ConogramUser[String]) =>
        setTheAdversaryActorWith(challengerUser.location.get)
        challengeController.askConfirmationForChallengeBy(challengerUser)
      case Reject => notifyViewChallenge(Reject)
      case NotifyPlayerToCloseMatch =>
        context.stop(self)
        challengeController.endOfMatch()
    }
  }

}

object ChildActor extends Enumeration {
  case class PeerValue(name: String) extends Val(name)

  type Peer = Value
  val Client: PeerValue = PeerValue("client")
  val Server = PeerValue("server")
  val LocalMatchActor = PeerValue("localMatchActor")
}
