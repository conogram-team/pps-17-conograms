package game.multiplayer.actor

import account.model.ConogramUser
import game.multiplayer.controller.MultiplayerMatchController

object ChallengeResponse {
  sealed trait EnumVal
  case class Accept(user: ConogramUser[String]) extends EnumVal
  case object Reject extends EnumVal
  case class StartMatch(matchController: MultiplayerMatchController[Boolean], user: ConogramUser[String]) extends EnumVal
}
case class SendToMatch(matchController: MultiplayerMatchController[Boolean],
                       challengerActorPath: String)
case class ChallengeRequestTo(ip: String, gridSize: (Int, Int), fullCellsNumber: Int)
case class ChallengeRequest(user: ConogramUser[String])
case class SelectedCellsResponse(cells: Map[(Int, Int), Boolean])