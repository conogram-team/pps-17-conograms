package game.multiplayer.actor

/**
  * Trait for the strategy whith which obtain the path of an actor.
  */
trait ActorPathBuilder {
  private val ActorParentPath = "user"
  private val ParentPartName = "player"
  private val ActorSystemName = "PlayerSystem"
  private val ActorBasePath = s"akka.tcp://$ActorSystemName"
  private val BasePath = s"akka://$ActorSystemName"
  private val Separator = "/"
  private val SystemAndHostSeparator = "@"
  private val MatchActorSystemName = "MatchSystem"
  private val MatchActorBasePath = s"akka.tcp://$MatchActorSystemName"
  private val EmptyIp = ""

  /**
    * Defines the strategy to obtaint the path of an actor.
    * @param peer The strict actor peer.
    * @param ip The ip of the actor.
    * @param children The children of the actor which has to be retreived.
    * @return
    */
  def getActorPathOf(peer: ChildActor.PeerValue, ip: Option[String], children: Option[Traversable[String]] = None): String = children match {
    case Some(child) =>
      getPath(ip).append(child.mkString(Separator)).toString()
    case None => getPath(ip) + peer.name
  }

  private def getPath(ip: Option[String]) = ip match {
    case Some(actorIp) =>
      new StringBuilder().append(ActorBasePath).append(SystemAndHostSeparator).append(actorIp).append(Separator)
        .append(ActorParentPath).append(Separator).append(ParentPartName).append(Separator)
    case None =>
      new StringBuilder().append(BasePath).append(Separator)
        .append(ActorParentPath).append(Separator).append(ParentPartName).append(Separator)

  }
}

case object ActorPathBuilder extends ActorPathBuilder
