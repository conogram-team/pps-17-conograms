package game.multiplayer.model

import java.awt.Color

import game.singleplayer.model.grid.Grid
import game.singleplayer.model.{MatchModel, Player}

trait MultiplayerMatch[T] extends MatchModel[T] {
  var challengersPlayers: Option[Set[Player]] = None
  private val _colorChallangers: Seq[Color] = Seq(ColorChallanger.TRUE.head, ColorChallanger.FALSE.head)

  /**
    * @return the colors of the challengers' selected cells
    */
  def getColorChallangers: Seq[Color] = _colorChallangers

  /**
    * @return challangers
    */
  def getChallengersPlayers: Set[Player]= challengersPlayers.get

  /** add the player to the list of challengers
    *
    * @param player of the challenger
    */
  def addPlayerInMatch(player: Player): Unit = challengersPlayers match {
    case Some(t) => challengersPlayers = Some(challengersPlayers.get + player)
    case None => challengersPlayers = Some(Set(player))
  }

  /**
    * @return the challenger player
    */
  def getChallenger: Player = challengersPlayers.get.head

  /**
    * @return a list of winners who have won the match (or drawn in the case of multiple players)
    */
  def getWinners : Set[Player] = {
    def isTheMatchEndedFor(player: Player, maxRightSelections: Int) =
      player.rightSelectionNumber == maxRightSelections && player.rightSelectionNumber == grid.getNumberOfFullCells
    def getMaxSelectionNumber = (challengersPlayers.get + localPlayer.get).map(p => p.rightSelectionNumber).max

    (challengersPlayers.get + localPlayer.get).filter(p => isTheMatchEndedFor(p, getMaxSelectionNumber) )
  }

  /**
    * @return if any player in the game won the match
    */
  def hasThePlayerWon : Boolean =
    (challengersPlayers.get + localPlayer.get).map( p=> p.rightSelectionNumber).sum == grid.getNumberOfFullCells

  /**
    * @return a list of winners who have won the match (or drawn in the case of multiple players)
    */
  def theWinnerIs : Seq[Player] = {
    val maxValuePlayer = (challengersPlayers.get + localPlayer.get).map(p => p.rightSelectionNumber).max
    challengersPlayers.get.filter(p => p.rightSelectionNumber == maxValuePlayer).toSeq
  }
}

object ColorChallanger extends Enumeration{
  type ColorChallanger = Value
  val FALSE = Seq(Color.RED, Color.ORANGE, Color.CYAN, Color.GRAY)
  val TRUE= Seq(Color.BLUE, Color.GREEN , Color.BLACK)
}

object MultiplayerMatchModel {
  def apply[T](providedGrid: Grid[T]): MultiplayerMatch[T] = new MultiplayerMatch[T] {
    protected override val _grid: Grid[T] = providedGrid
  }
}