package game.multiplayer.view

import game.multiplayer.view.Confirmation.Confirmation
import game.singleplayer.model.Difficulty.Difficulty
import game.singleplayer.view.MenuView

/** View for the opponent selection and challenge */
trait MultiplayerMenuView extends MenuView {
  /** Add an observer that will be notified when the user request to challenge a random user */
  def addORandomChallengeObserver( observer: () => Unit): Unit
  /** Add an observer that will be notified when the user request to challenge a defined user
    *  the observer will receive the username of the opponent
    */
  def addUserChallengeObserver( observer: String => Unit): Unit
  /** Display a message to the user */
  def showMessage(title: String, message: String): Unit
  /** Ask the user for a confirmation of type 'yes/no', then executes the task with the user response */
  def showConfirmation(title: String, message: String, task: Confirmation => Unit): Unit
  /** get the number of rows set by the user */
  def getGridRows: Int
  /** get the number of columns set by the user */
  def getGridColumns: Int
  /** get the difficulty set by the user */
  def getGridDifficulty: Difficulty
}

/** A user response to the confirmation request
  *  yes -> Confirmed
  *  no -> Rejected
  */
object Confirmation extends Enumeration {
  type Confirmation = Value
  val Confirmed, Rejected = Value
}