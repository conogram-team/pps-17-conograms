package game.multiplayer.view

//noinspection ScalaStyle
import java.awt.Color

import Confirmation.Confirmation
import game.singleplayer.model.Difficulty
import game.singleplayer.model.Difficulty.Difficulty
import game.singleplayer.view.MenuView

import scala.swing.event.ButtonClicked
import scala.swing.{BoxPanel, Button, Dialog, Label, Swing, TextField}
import scala.util.Try

/** A GUI for multiplayer games that extends the single player menu functionality
  *   Add the view interface for challenge other users
  */
class MultiplayerMenuGUI extends MenuView with MultiplayerMenuView {

  val mainBox = contents.head.asInstanceOf[BoxPanel].contents

  val opponentLabel = "Opponent:"
  val spaceAfterSinglePlayer = 100

  val opponentTextField = new TextField
  val randomChallengeButton = new Button("Challenge random opponent")
  val opponentChallengeButton = new Button("Challenge opponent")

  private var randomChallengeObservers: Seq[() => Unit] = Seq()
  private var opponentChallengeObservers: Seq[String => Unit] = Seq()

  restrictHeight(opponentTextField)
  mainBox += (
    Swing.VStrut(spaceAfterSinglePlayer),
    new Label(opponentLabel),
    opponentTextField,
    opponentChallengeButton,
    randomChallengeButton)

  listenTo(randomChallengeButton, opponentChallengeButton)
  reactions += {
    case ButtonClicked(`randomChallengeButton`) if checkDimension() => notifyRandomChallengeObserver()
    case ButtonClicked(`opponentChallengeButton`) if checkDimension() => notifyOpponentChallengeObserver()
  }

  //noinspection ScalaStyle
  peer.setLocationRelativeTo(null)

  override def addORandomChallengeObserver(observer: () => Unit): Unit = {
    randomChallengeObservers = randomChallengeObservers :+ observer
  }

  override def addUserChallengeObserver(observer: String => Unit): Unit = {
    opponentChallengeObservers = opponentChallengeObservers :+ observer
  }

  override def showMessage(title: String, message: String): Unit = {
    Swing.onEDT {
      Dialog.showMessage(this, message, title)
    }
  }

  override def showConfirmation(title: String, message: String, task: Confirmation => Unit): Unit = {
    Swing.onEDT {
      Dialog.showConfirmation(this, message, title) match {
        case Dialog.Result.Yes => task(Confirmation.Confirmed)
        case _ => task(Confirmation.Rejected)
      }
    }
  }

  override def getGridDifficulty: Difficulty = Difficulty.withName(boxValueDifficulty.selected.get.text)

  override def getGridRows: Int = parseInt(rowsTextField.text)

  override def getGridColumns: Int = parseInt(columnsTextField.text)

  private def parseInt(text: String): Int = Try(text.toInt).getOrElse(0)

  private def checkDimension(): Boolean = {
    if (getGridRows > 0 && getGridRows<30 && getGridColumns > 0 && getGridColumns<30) {
      chooseDimensionLabel.foreground = Color.BLACK
      true
    } else {
      chooseDimensionLabel.foreground = Color.RED
      false
    }
  }

  private def notifyRandomChallengeObserver(): Unit = { randomChallengeObservers.foreach(_ ()) }

  private def notifyOpponentChallengeObserver(): Unit = {
    opponentChallengeObservers.foreach(_ (opponentTextField.text))
  }
}
/** Factory for [[MultiplayerMenuGUI]] */
object MultiplayerMenuGUI {
  /** Produces a [[MultiplayerMenuGUI]]: Menu for match settings and challenges, see [[MultiplayerMenuGUI]] class */
  def apply(): MultiplayerMenuGUI = new MultiplayerMenuGUI()
}
