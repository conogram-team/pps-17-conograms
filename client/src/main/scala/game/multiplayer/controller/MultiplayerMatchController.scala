package game.multiplayer.controller

import java.awt.Color
import java.sql.Timestamp
import java.time.Instant

import akka.actor.{ActorSelection, ActorSystem}
import game.multiplayer.actor.{ActorPathBuilder, CloseMessage, MessageCellSelected, ChildActor}
import account.model.service.support.Network
import game.multiplayer.model.{MultiplayerMatch, MultiplayerMatchModel}
import game.singleplayer.controller.MatchController
import game.singleplayer.model.Player
import game.singleplayer.model.grid.Grid

import scala.language.postfixOps

trait MultiplayerMatchController[T] extends MatchController[T] {
  private val actorChildren: Option[Traversable[String]] = Some(Seq("localMatchActor", "MatchClient"))
  private var actorMatch: Option[ActorSelection] = None

  /** A method to add challengers to the game
    *
    * @param player to add at challengers in MatchModel
    */
  def addPlayerInMatch(player: Player): Unit = matchModel.addPlayerInMatch(player)

  /**
    * @see [[game.multiplayer.model.MultiplayerMatch]]
    */
  def getChallenger: Player = matchModel.getChallenger

  /**
    * Add a cell selected by a specified player to the selected cells collection.
    */
  def addSelectedCell(position: (Int, Int), supposedStatus: T, player: Player): Unit = {
    super.selectCellIn(position, supposedStatus, player)
    if (localPlayer.get == player) _matchView.get.updateLives(player.getAvailableLives)
    _matchView.get.updateGrid()
  }


  override def getWinners: Set[Player]= matchModel.getWinners


  /**
    * @see [[game.singleplayer.controller.MatchController]]
    */
  override def getSelectedCells(player: Player): Map[(Int, Int), (T, Color)] = {

    def isRightSelection(point: (Int, Int)): Color = {
      if (matchModel.getCellStatusIn(point).get == true) matchModel.getColorChallangers.head
      else matchModel.getColorChallangers.tail.head
    }

    super.getSelectedCells() ++ matchModel.challengersPlayers.get.flatMap(p =>
      p.getSelectedCells map (cell => cell -> (matchModel.getCellStatusIn(cell).get, isRightSelection(cell))))
  }

  /**
    * @see [[game.singleplayer.controller.MatchController]]
    */
  override def selectCellIn(position: (Int, Int), supposedStatus: T, player: Player): Unit = {
    actorMatch = Some(Network.applicationActorSystem.get.actorSelection(ActorPathBuilder.getActorPathOf(ChildActor.Client, None, actorChildren)))
    if (!getSelectedCells(player).contains(position)) {
      actorMatch.get ! MessageCellSelected(position, Timestamp.from(Instant.now()))
    }
  }

  override def openMenuView(): Unit = {
    actorMatch = Some(Network.applicationActorSystem.get.actorSelection(ActorPathBuilder.getActorPathOf(ChildActor.Client, None, actorChildren)))
    actorMatch.get ! CloseMessage
  }

  override protected def matchModel: MultiplayerMatch[T]
}

object MultiplayerMatchController {
  def apply[T](grid: Grid[T]): MultiplayerMatchController[T] = new MultiplayerMatchController[T] {
    override val matchModel: MultiplayerMatch[T] = MultiplayerMatchModel[T](grid)
  }
}
