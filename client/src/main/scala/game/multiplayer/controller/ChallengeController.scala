package game.multiplayer.controller

import account.Account
import account.model.service.support.Network
import account.model.ConogramUser
import game.multiplayer.actor.ChallengeResponse.{Accept, EnumVal, Reject, StartMatch}
import game.multiplayer.actor._
import game.multiplayer.view.{Confirmation, MultiplayerMenuView}
import game.singleplayer.view.MatchViewNonogram
import game.singleplayer.view.builder.MatchViewBuilder
import rest.user.api.UserApi
import rest.user.model.User

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}
import scala.swing.Swing
import scala.util.{Failure, Success}


/**
  * Controller that deals with setting up the game,
  * contacting another user to challenge.
  */
sealed trait ChallengeController {

  /**
    * A method to challenge a user whose username is known
    *
    * @param username of the user to challenge
    */
  def challengePlayer(username: String): Unit


  /**
    * A method to challenge any user provided by the system
    */
  def challengeRandomPlayer(): Unit


  /**
    * A method that notifies the view based on the response obtained from the opponent
    *
    * @param challengeResult response obtained from the opponent
    */
  def notifyViewOf(challengeResult: EnumVal): Unit


  /**
    * A method that asks the user to accept the challenge requested by another user
    *
    * @param user user who requested the challenge
    */
  def askConfirmationForChallengeBy(user: ConogramUser[String]): Unit


  /**
    * A method that makes the current user log out
    */
  def logout(): Unit

  /**
    * A method to handle the end of the match
    */
  def endOfMatch(): Unit

}


/**
  * @see [[ChallengeController]]
  */
private class DefaultChallengeController(val menuView: MultiplayerMenuView) extends ChallengeController {
  private val ActorName = "player"
  private val userApi = new UserApi()
  private val InputErrorTitle = "Username Wrong"
  implicit val challengeController: ChallengeController = this
  private val player = Network.applicationActorSystem.get.actorOf(ChallengeManager.props, ActorName)


  /**
    * @see [[ChallengeController]]
    * @param username of the user to challenge
    */
  override def challengePlayer(username: String): Unit =
    if (this.checkUsername(username) & this.checkUser(username)) {
      this findChallenger this.userApi.getIpByUsernameAsync(username)
    }


  /**
    * @see [[ChallengeController]]
    */
  override def challengeRandomPlayer(): Unit = {
    val usernameOfCurrentUser: String = Account.currentUser.get.username
    this findChallenger this.userApi.getRandomUserAsync(usernameOfCurrentUser).map(user => user.ip.get)
  }


  /**
    * @see [[ChallengeController]]
    */
  override def notifyViewOf(challengeResponse: EnumVal): Unit = challengeResponse match {
    case Accept(_) =>
      Swing.onEDT {
        this.menuView.close()
      }
    case Reject =>
      val ChallengerAnswerTitle = "Challenger's answer"
      val ChallengerAnswerMessage = "The challenger rejected the challenge!"
      this.menuView.showMessage(ChallengerAnswerTitle, ChallengerAnswerMessage)
    case StartMatch(matchController: MultiplayerMatchController[Boolean], _) =>
      this.startOfMatch(matchController)
  }


  /**
    * @see [[ChallengeController]]
    */
  override def askConfirmationForChallengeBy(user: ConogramUser[String]): Unit = {
    val ChallengeRequestTitle = "Challenge Request"
    val ChallengeRequestMessage = "The player %s wants to challenge you"

    this.menuView.showConfirmation(ChallengeRequestTitle, ChallengeRequestMessage.format(user.username), {
      case Confirmation.Confirmed => {
        player ! Accept(Account.currentUser.get)
      }
      case _ => player ! Reject
    })
  }

  /**
    * @see [[ChallengeController]]
    */
  override def logout(): Unit =
    if (Account.currentUser.isDefined) {
      val username = Account.currentUser.get.username
      val ip = Account.currentUser.get.location
      this.userApi.logoutUserAsync(User(username = username, ip = ip))
    }


  /**
    * @see [[ChallengeController]]
    */
  override def endOfMatch(): Unit = menuView.display()


  private def checkUsername(username: String): Boolean = {
    val inputErrorMessage = "Username should not be empty"
    val isEmpty = username.trim.isEmpty
    if (isEmpty) this.menuView.showMessage(InputErrorTitle, inputErrorMessage)
    !isEmpty
  }

  
  private def checkUser(username: String): Boolean = {
    val localUserErrorMessage = "You cannot challenge yourself!"
    val isLocalUser = Account.currentUser.isDefined && Account.currentUser.get.username == username
    if (isLocalUser) this.menuView.showMessage(InputErrorTitle, localUserErrorMessage)
    !isLocalUser
  }


  private def findChallenger(future: Future[String]): Unit = future onComplete {
    case Success(playerIpAddress) =>
      val WaitingTitle = "Waiting for the challenger's response"
      val WaitingMessage = "..."

      val rows: Int = menuView.getGridRows
      val columns: Int = menuView.getGridColumns
      val fullCellsNumber: Int = menuView.getGridDifficulty.fullCellsOf(rows, columns)

      player ! ChallengeRequestTo(playerIpAddress, (rows, columns), fullCellsNumber)
      this.menuView.showMessage(WaitingTitle, WaitingMessage)
    case Failure(_) =>
      val UserNotFoundMessage = "Would you try again?"
      val UserNotFoundTitle = "Challenger Not Found"
      this.menuView.showMessage(UserNotFoundTitle, UserNotFoundMessage)
  }


  protected def runInSafeContext(task: => Unit): Unit =
    ExecutionContext.global.execute(new Runnable {
      override def run(): Unit = task
    })


  protected def startOfMatch(matchController: MultiplayerMatchController[Boolean]): Unit = {
    runInSafeContext {
      val viewSize: (Int, Int) = matchController.getGridSize
      val builder: MatchViewBuilder[Boolean] = new MatchViewBuilder[Boolean](viewSize._1, viewSize._2, true)
      val matchView = new MatchViewNonogram[Boolean](builder.build().get, matchController)
      matchController.matchView_=(matchView)
    }

    menuView.close()
  }

}


/**
  * Companion object for creating the ChallengeController
  */
object ChallengeController {
  def apply(menuView: MultiplayerMenuView): ChallengeController = new DefaultChallengeController(menuView)
}
