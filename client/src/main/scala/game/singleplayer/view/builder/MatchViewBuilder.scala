package game.singleplayer.view.builder

/**
  * @param rows             the number of rows in the grid
  * @param columns          the number of columns in the grid
  * @param defaultStatus   default state of the cell
  */
case class ParameterMatchView[T](rows: Int, columns: Int, defaultStatus: T)

/**builder of ParameterMatchView
  * @param rows             the number of rows in the grid
  * @param columns          the number of columns in the grid
  * @param defaultStatus   default state of the cell
  */
class MatchViewBuilder[T](rows: Int, columns: Int, defaultStatus: T) {
  def build(): Option[ParameterMatchView[T]] = {
    if (!(defaultStatus == null || rows == 0 || columns == 0))
      Some(ParameterMatchView[T](rows, columns, defaultStatus))
    else None
  }
}
