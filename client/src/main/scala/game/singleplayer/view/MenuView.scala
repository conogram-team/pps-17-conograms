package game.singleplayer.view

import java.awt.Color
import game.singleplayer.controller.MenuController
import game.singleplayer.model.Difficulty
import scala.swing._
import scala.swing.event.ButtonClicked

class MenuView extends MainFrame with View {

  def restrictHeight(s: Component) {
    s.maximumSize = new Dimension(Short.MaxValue, s.preferredSize.height)
  }

  val that: MenuView = this
  private val dimensionMenu: Int = 400
  private val dimensionColumnTextField = 5
  private val sizeComponent = 10
  private val borderDim = 10
  private val valueLayout = 0.0

  preferredSize = new Dimension(dimensionMenu, dimensionMenu)
  title = "Nonograms"
  val numberPattern = "[0-9]+"
  val easyValue = new RadioButton(Difficulty.Low.toString())
  val mediumValue = new RadioButton(Difficulty.Medium.toString())
  val difficultyValue = new RadioButton(Difficulty.High.toString())
  easyValue.selected = true
  val boxValueDifficulty = new ButtonGroup(easyValue, mediumValue, difficultyValue)
  val rowsTextField = new TextField {
    columns = dimensionColumnTextField
  }
  val columnsTextField = new TextField {
    columns = dimensionColumnTextField
  }
  val chooseDimensionLabel = new Label("Choose dimensions")

  val controller = MenuController(this)

  restrictHeight(rowsTextField)
  restrictHeight(columnsTextField)

  contents = new BoxPanel(Orientation.Vertical) {

    contents += new Label("Choose difficulty")
    contents += Swing.VStrut(sizeComponent)
    contents += new BoxPanel(Orientation.Horizontal) {
      contents += easyValue
      contents += mediumValue
      contents += difficultyValue
    }
    contents += Swing.VStrut(sizeComponent)
    contents += chooseDimensionLabel
    contents += new BoxPanel(Orientation.Horizontal) {
      contents += new Label("row")
      contents += Swing.HStrut(sizeComponent)
      contents += rowsTextField
      contents += Swing.HStrut(sizeComponent)
      contents += new Label("columns")
      contents += Swing.HStrut(sizeComponent)
      contents += columnsTextField
    }
    val submitButton = new Button("submit")
    submitButton.preferredSize
    contents += Swing.VStrut(sizeComponent)
    contents += submitButton
    listenTo(submitButton)
    reactions += {
      case ButtonClicked(`submitButton`) =>
        if (rowsTextField.text.isEmpty || columnsTextField.text.isEmpty
          || !verifyNumber(rowsTextField.text, columnsTextField.text)) {
          chooseDimensionLabel.foreground = Color.RED
        } else {
          val rows = rowsTextField.text.toInt
          val columns = columnsTextField.text.toInt
          val fullCellNumber = Difficulty
            .withName(boxValueDifficulty.selected.get.text)
            .fullCellsOf(rows, columns)
          val controller: MenuController = MenuController(that)
          controller.startMatch(rows, columns, fullCellNumber)
        }
    }
    for (e <- contents)
      e.xLayoutAlignment = 0.0
    border = Swing.EmptyBorder(borderDim, borderDim, borderDim, borderDim)
  }

  this.openView()

  /**
    * @see [[View]]
    */
  def openView(): Unit = Swing.onEDT {
    this.visible_=(true)
  }

  /**
    * @see [[View]]
    */
  def closeView(): Unit = Swing.onEDT {
    this.visible_=(false)
  }

  /**
    * A method to show the view
    */
  override def display(): Unit = Swing.onEDT {
    this.visible_=(true)
  }

  /**
    * A method to close the view
    */
  override def close(): Unit = Swing.onEDT {
    this.visible_=(false)
  }

  private def verifyNumber(row: String, column: String): Boolean = {
    def checkIsContainsError(axis: String): Boolean = axis.matches(numberPattern)

    checkIsContainsError(row) && checkIsContainsError(column)
  }
}
