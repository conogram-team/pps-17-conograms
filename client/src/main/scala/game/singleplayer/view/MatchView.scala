package game.singleplayer.view

import java.awt.{Color, Graphics}
import java.awt.geom.Line2D

import account.Account
import game.singleplayer.model.Player
import game.singleplayer.controller.MatchController
import game.singleplayer.model.StatusOfMatch.StatusOfMatch
import game.singleplayer.model.StatusOfMatch
import game.singleplayer.view.builder.ParameterMatchView
import javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE

import scala.collection.mutable
import scala.swing.event.MouseClicked
import scala.swing.{BorderPanel, BoxPanel, Dialog, Dimension, FlowPanel, Graphics2D, Label, MainFrame, Orientation, Point, ScrollPane, SimpleSwingApplication, Swing}

/** Class used to create the game grid
  *
  * @param parameter of the grid
  * @param matchController controller of match
  */
class MatchViewNonogram[T]( private val parameter: ParameterMatchView[T],
                            private val matchController: MatchController[T]) extends ConogramView {
  private val defaultNumberOfLives = 3
  private var numberOfLives = defaultNumberOfLives
  private val cellSize = 25
  private val hintsDimension = matchController.getMaxHintsNumber
  private val emptyBorderGrid = 50
  private val heightGrid = (parameter.rows + hintsDimension) * cellSize
  private val widthGrid = (parameter.columns + hintsDimension) * cellSize
  private val gridDimension: Dimension = new Dimension(widthGrid, heightGrid)
  implicit val player: Player= matchController.localPlayer.get

  GUINonogram.top.visible_=(true)

  /**
    * @inheritdoc
    */
  override def updateLives(lives: Int): Unit = {
    this.numberOfLives = lives
    GUINonogram.livesNumberLabel.text = numberOfLives.toString
    GUINonogram.livesNumberLabel.repaint()
  }

  /**
    * @inheritdoc
    */
  override def updateGrid(): Unit = {
    GUINonogram.gridPanel.repaint()
    val status= matchController.statusOfMatch
    if (status != StatusOfMatch.Playing) showMatchOutcome(matchController.statusOfMatch)
  }

  /**
    * @inheritdoc
    */
  override def showMatchOutcome(state: StatusOfMatch): Unit = {
    if (state == StatusOfMatch.Win) {
      GUINonogram.updatePopUp(state, matchController.getWinners.map(p=>p.getUser.username).mkString(" "))
      matchController.openMenuView()
    } else if (state == StatusOfMatch.Loss) {
      GUINonogram.updatePopUp(state, Account.currentUser.get.username)
      matchController.openMenuView()
    }
    GUINonogram.top.dispose()
  }


  /**
    * Create the match view
    */
  object GUINonogram extends SimpleSwingApplication {
    val defaultFrameSize = 800

    def updatePopUp(state: StatusOfMatch, player: String): Unit = state match {
      case StatusOfMatch.Win => Dialog.showConfirmation(null, "The winner is " + player + "!", optionType = Dialog.Options.Default, title = "win")
      case StatusOfMatch.Loss => Dialog.showConfirmation(null, player + " loss match!", optionType = Dialog.Options.Default, title = "loss")
    }

    def top: MainFrame = new MainFrame {
      title = "nonograms"
      contents = matchPanel
      size = new Dimension(defaultFrameSize, defaultFrameSize)
      peer.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE)
      override def closeOperation() { showCloseDialog() }

      private def showCloseDialog() {
        Dialog.showConfirmation(
          parent = null,
          title = "Exit",
          message = "Are you sure you want to quit?"
        ) match {
          case Dialog.Result.Ok =>
            matchController.openMenuView()
            peer.dispose()
          case _ => ()
        }
      }
      peer.setLocationRelativeTo(null)
    }

    val livesNumberLabel: Label = new Label {
      text = String.valueOf(numberOfLives)
    }

    val livesPanel: BorderPanel = new BorderPanel {
      val livesLabel: Label = new Label {
        text = "Number of lives available for " + Account.currentUser.get.username + ":"
      }

      val panelNorth = new FlowPanel(livesLabel, livesNumberLabel)
      add(panelNorth, BorderPanel.Position.West)
    }

    val gridPanel: BorderPanel =new GridNonogram(gridDimension){
      preferredSize_=(gridDimension)
      listenTo(mouse.clicks)
      reactions += {
        case e: MouseClicked =>
          val point = calculatePointGrid(e.point)
          if (isPointIntoGrid(point)) {
            matchController.selectCellIn((point.y, point.x), parameter.defaultStatus)
            updateLives(player.getAvailableLives)
            updateGrid()
          }
      }
    }

    val scroll: ScrollPane= new ScrollPane{
      contents= gridPanel
    }

    val matchPanel: BorderPanel = new BorderPanel {
      border = Swing.EmptyBorder(emptyBorderGrid)
      layout += (livesPanel -> BorderPanel.Position.North)
      layout += (scroll -> BorderPanel.Position.Center)
    }
    /** Class used to create nonogram grid
      *
      * @param dimension dimension of the grid
      */
    class GridNonogram(dimension: Dimension) extends BorderPanel {
      private val nCellsX = dimension.width / cellSize
      private val nCellsY = dimension.height / cellSize
      private var pointList: mutable.Map[(Int, Int), (T, Color)] = mutable.Map()
      private val verticalHints: Map[Int, Seq[(Int, T)]] = matchController.getHints._1
      private val horizontalHints: Map[Int, Seq[(Int, T)]] = matchController.getHints._2

      /**
        * @param g the shape where the grid is drawn
        */
      override def paintComponent(g: Graphics2D): Unit = {
        super.paintComponent(g)
        pointList = pointList ++ matchController.getSelectedCells()
        g.setBackground(Color.WHITE)
        g.clearRect(0, 0, dimension.width, dimension.height)
        g.setColor(Color.BLUE)
        drawVerticalHints(g)
        drawHorizontalHints(g)
        drawInitialRowColumn(g)
        drawRowGrid(g)
        drawColumnGrid(g)
        drawPointGrid(g)
      }

      /** Draw hints of the game into row
        *
        * @param g use to draw hints
        */
      private def drawVerticalHints(g: Graphics2D): Unit = {
        var y = cellSize * hintsDimension + cellSize / 2
        for (i <- 0 to parameter.rows) {
          var x = cellSize / 2
          if (verticalHints.get(i).isDefined) {
            verticalHints(i).foreach(t => {
              g.drawString("" + t._1, x, y)
              x += cellSize
            })
            y += cellSize
          }
        }
      }

      /** Draw hints of the game into column
        *
        * @param g use to draw hints
        */
      private def drawHorizontalHints(g: Graphics2D): Unit = {
        var x = cellSize * hintsDimension + cellSize / 2
        for (i <- 0 to parameter.columns) {
          var y = cellSize / 2
          if (horizontalHints.get(i).isDefined) {
            horizontalHints(i).foreach(t => {
              g.drawString("" + t._1, x, y)
              y += cellSize
            })
            x += cellSize
          }
        }
      }

      /** Draw shape grid
        *
        * @param g use to draw the rows of the grid
        */
      private def drawRowGrid(g: Graphics): Unit = {
        var x = cellSize * hintsDimension
        for (i <- hintsDimension to nCellsX) {
          g.drawLine(x, 0, x, dimension.height)
          x += cellSize
        }
      }

      /** Draw shape grid
        *
        * @param g use to draw the column of the grid
        */
      private def drawColumnGrid(g: Graphics2D): Unit = {
        var y = cellSize * hintsDimension
        for (j <- hintsDimension to nCellsY) {
          g.drawLine(0, y, dimension.width, y)
          y += cellSize
        }
      }

      /** Draw shape grid
        *
        * @param g use to draw the column of the grid
        */
      private def drawInitialRowColumn(g: Graphics2D): Unit = {
        g.drawLine(0, 0, 0, dimension.height)
        g.drawLine(0, 0, dimension.width, 0)
      }

      /** Draw shape grid
        *
        * @param g use to draw the point into the grid
        */
      private def drawPointGrid(g: Graphics2D): Unit = {
        if (pointList.nonEmpty) pointList.foreach({
          case (key, value) => drawSelectionPoint(new Point(key._2, key._1), g, value._1, value._2)
        })
      }
    }

    /** Verify that selected point is into the nonogram grid
      *
      * @param point selected point
      * @return if the point is into the nonogram grid
      */
    private def isPointIntoGrid(point: Point) = point.x >= 0 && point.x < parameter.columns && point.y >= 0 && point.y < parameter.rows

    /** Calculate selected point in the grid starting from a real point of graphics
      *
      * @param point selectioned point into graphic
      * @return point of the grid
      */
    private def calculatePointGrid(point: Point): Point = {
      val pointGrid = new Point(0, 0)
      pointGrid.x += calculatePositionOf(point.x)
      pointGrid.y += calculatePositionOf(point.y)
      pointGrid
    }

    /** Calculate selected point of the grid starting from the coordinates of a selected point in the graphic
      *
      * @param point selectioned point into graphic
      */
    private def calculatePositionOf(point: Int): Int = point / cellSize - hintsDimension

    /** Draw point into the grid (fill cell if true, 'x' if false)
      *
      * @param g use to draw the point into the grid
      */
    private def drawSelectionPoint(point: Point, g: Graphics2D, value: T, color: Color): Unit = value match {
      case true => fillCellAt(point, g, color)
      case false => drawCrossAt(point, g, color)
    }

    /** Fill cell into the grid
      *
      * @param g use to draw the point into the grid
      */
    private def fillCellAt(point: Point, g: Graphics2D, color: Color): Unit = {
      g.setColor(color)
      val x1 = calculatePointFromSelectedCell(point.x)
      val y1 = calculatePointFromSelectedCell(point.y)
      g.fillRect(x1, y1, cellSize, cellSize)
    }

    /** Draw a cross into the grid cell
      *
      * @param g use to draw the point into the grid
      */
    private def drawCrossAt(point: Point, g: Graphics2D, color: Color): Unit = {
      val x = calculatePointFromSelectedCell(point.x)
      val y = calculatePointFromSelectedCell(point.y)
      g.setColor(color)
      g.draw(new Line2D.Double(x, y, x + cellSize, y + cellSize))
      g.draw(new Line2D.Double(x + cellSize, y, x, y + cellSize))
    }

    /** Calculate real point of graphics starting from a selected point in the grid
      *
      * @param point selectioned point by user
      */
    private def calculatePointFromSelectedCell(point: Int) = (point + hintsDimension) * cellSize
  }

}


object BooleanMatchView {
  def apply(parameterMatchView: ParameterMatchView[Boolean], matchController: MatchController[Boolean]): ConogramView =
    new MatchViewNonogram[Boolean](parameterMatchView, matchController)
}