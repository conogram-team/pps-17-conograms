package game.singleplayer.view

/**
  * An interface for a generic view
  */
trait View {

  /**
    * A method to show the view
    */
  def display(): Unit

  /**
    * A method to close the view
    */
  def close(): Unit

}
