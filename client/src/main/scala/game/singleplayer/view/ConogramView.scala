package game.singleplayer.view

import game.singleplayer.model.StatusOfMatch.StatusOfMatch

/**
  *an interface for a generic view
  */
trait ConogramView {

  /**
    * @param lives lives available by the user
    */
  def updateLives(lives: Int): Unit

  /**
    * Update grid
    */
  def updateGrid(): Unit

  /**
    * Show if the match has been won or lost
    */
  def showMatchOutcome(state: StatusOfMatch): Unit
}