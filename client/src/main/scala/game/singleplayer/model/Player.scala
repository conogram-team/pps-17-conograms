package game.singleplayer.model

import java.awt.Color

import account.model.ConogramUser

import scala.collection.mutable.ListBuffer

trait Player {

  /**
    * @return Returns who is the logged user that is performing a match.
    */
  def getUser: ConogramUser[String]

  /**
    * @return Returns the number of lives of the player whithin the match.
    */
  def getAvailableLives: Int

  /**
    * Decrements the available lives of the player
    */
  def decrementAvailableLives(): Unit

  /**
    * @return Returns the cells selected by the player whithin the match.
    */
  def getSelectedCells: Traversable[(Int, Int)]

  /**
    * Adds a cell selected by the player to the selected cells collection.
    * @param position The position of the selected cell.
    */
  def addSelectedCellIn(position: (Int, Int)): Unit

  /**
    * Increment the number of right selections.
    */
  def incrementRightSelectionNumber(): Unit

  /**
    * @return Returns the number of right selection by the player whithin the match.
    */
  def rightSelectionNumber: Int

  /**
    * @return Returns the colors, both of a right cell and a wrong cell, of the player.
    */
  def getPlayerColors: (Color, Color)

}

case class PlayerInGame(private val _user: ConogramUser[String], private var _availableLives: Int) extends Player {
  private val _selectedCells: ListBuffer[(Int, Int)] = ListBuffer.empty
  private var _rightSelectionNumber: Int = 0
  private val IncrementValue = 1
  private val DecrementValue = 1
  private val _playerColors: (Color, Color) = (Color.BLACK, Color.RED)

  override def getUser: ConogramUser[String] = _user

  override def decrementAvailableLives(): Unit = _availableLives = _availableLives - DecrementValue

  override def getAvailableLives: Int = _availableLives

  override def getSelectedCells: Traversable[(Int, Int)] = _selectedCells.toList

  override def addSelectedCellIn(position: (Int, Int)): Unit = _selectedCells.append(position)

  override def incrementRightSelectionNumber(): Unit = _rightSelectionNumber= rightSelectionNumber + IncrementValue

  override def rightSelectionNumber: Int = _rightSelectionNumber

  override def getPlayerColors: (Color, Color) = _playerColors
}

object Player {
  val DefaultNumberOfAvailableLives = 3

  def apply(user: ConogramUser[String], livesAvailable: Int = DefaultNumberOfAvailableLives): Player
  = PlayerInGame(user, livesAvailable)
}


