package game.singleplayer.model

import java.awt.Color

import game.singleplayer.model.StatusOfMatch.StatusOfMatch
import game.singleplayer.model.grid.Axis.Axis
import game.singleplayer.model.grid.{Axis, Grid}

/**
  * Models the state of a Nonogram match
  * @tparam T The type of the grid's cells, which could be, for instance, boolean or colored.
  */
trait MatchModel[T] {

  private var _localPlayer: Option[Player] = None
  private var _statusOfMatch: StatusOfMatch = StatusOfMatch.Playing
  protected val _grid: Grid[T]

  /**
    * @param position The position of the cell into the grid expressed as a tuple of row and column.
    * @param supposedStatus The supposed status of the cell selected by the player.
    * @return Returns whether the status supposed by the player by clicking into a cell is correct or not.
    */
  def checkCellStatusIn(position: (Int, Int), supposedStatus: T): Boolean = {
    grid.getCellStatusIn(position) match {
      case Right(status) => status == supposedStatus
      case _ => false
    }
  }

  /**
    * @param position the position of the cell which the status is needed.
    * @return Returns the status of a cell in a provided position within the grid.
    */
  def getCellStatusIn(position: (Int, Int)): Option[T] = grid.getCellStatusIn(position) match {
    case Right(status) => Some(status)
    case _ => None
  }

  /**
    * @param axis The direction of the hints (for example they could refer to the grid in a
    *                  vertically or horizontally point of view).
    * @return Returns the hints to allow the player suppose a cell status.
    */
  def getGridHints(axis: Axis): Map[Int, Seq[(Int, T)]] =  grid.getAllHints(axis)

  /**
    * @param player The player to check whether has won.
    * @return Whether the provided player has won.
    */
  def hasThePlayerWon(player: Player = localPlayer.get) : Boolean =
    player.rightSelectionNumber == grid.getNumberOfFullCells

  /**
    * @return Returns the max number of hints within a grid
    */
  def getMaxHintsNumber: Int = {
    var max = 0
    (getGridHints(Axis.Vertical).values ++ getGridHints(Axis.Horizontal).values)
    .foreach(s => if (s.size > max) max = s.size)
    max
  }

  def getGridSize: (Int, Int) = grid.getSize

  /**
    * @return Returns the colors for the right selection of a cell or the wrong one for the current local player.
    */
  def getLocalPlayerColors: (Color, Color) = localPlayer.get.getPlayerColors

  /**
    * @return Returns the state of a match which could be, for instance, running/win/loss.
    */
  def statusOfMatch: StatusOfMatch = _statusOfMatch

  def statusOfMatch_= (status: StatusOfMatch): Unit = _statusOfMatch = status

  /**
    * @return Returns the current player within the match.
    */
  def localPlayer: Option[Player] = _localPlayer

  def localPlayer_=(localPlayer: Player): Unit = _localPlayer = Some(localPlayer)

  /**
    * @return Returns the grid of cells within a match.
    */
  def grid: Grid[T] = _grid
}

/**
  * Enum for the possible status of match.
  */
case object StatusOfMatch extends Enumeration {
  type StatusOfMatch = Value
  val Playing, Win, Loss: StatusOfMatch = Value
}

/**
  * Factory for [[game.singleplayer.model.MatchModel]] instances.
  */
object MatchModel {

  /**
    * Creates the model of a Nonogram match using a provided grid.
    * @param providedGrid The grid that will be used in a match.
    * @tparam T The type of the grid's cells, which could be, for instance, boolean or colored.
    * @return Returns the match model.
    */
  def apply[T](providedGrid: Grid[T]): MatchModel[T] = new MatchModel[T] {
    protected override val _grid: Grid[T] = providedGrid
  }
}

