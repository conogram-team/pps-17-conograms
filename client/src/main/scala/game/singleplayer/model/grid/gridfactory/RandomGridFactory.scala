package game.singleplayer.model.grid.gridfactory

import game.singleplayer.model.grid.{Axis, Grid}
import utility.prolog.ScalaToProlog._

import scala.reflect.ClassTag

/** Factory for [[Grid]] instances. */
trait RandomGridFactory[GridType] {
  /** Creates a random grid with given dimensions and number of full cells.
    *
    *  @param rows the number of rows in the grid
    *  @param columns the number of columns in the grid
    *  @param fullCells the amount of full cells that must populate the grid
    */
  def apply(rows: Int, columns: Int, fullCells: Int): Grid[GridType]
}

/** Template for definition of factory for [[Grid]] instances.
  *  it only needs the definition of defaultStatus and generator
  */
trait RandomGridFactoryTemplate[GridType] extends RandomGridFactory[GridType] {

  override def apply(rows: Int, columns: Int, fullCells: Int): Grid[GridType] = {
    parameterCheck(rows, columns, fullCells)
    implicit val default: GridType = defaultStatus
    val grid = Grid[GridType](rows, columns)
    val engine = generator.produceGrid(rows, columns, fullCells)
    fillGrid(grid, engine)
  }
  /** The neutral value of grid type parameter */
  protected def defaultStatus: GridType
  /** The [[PrologGrid]] to be used for fill and extract values from the Prolog engine */
  protected def generator: PrologGrid[GridType]
  /** ClassTag needed to find runtime type of grid */
  protected implicit def gridTypeTag: ClassTag[GridType]

  private def parameterCheck(rows: Int, columns: Int, fullCells: Int): Unit = {
    require(rows > 0)
    require(columns > 0)
    require(fullCells >= 0)
    require(rows * columns >= fullCells)
  }

  private def fillGrid(baseGrid: Grid[GridType], engine: Engine): Grid[GridType] = {
    var grid = baseGrid
    for(cell <- generator.getCellFromPrologGrid(engine)) {
      grid = grid.setCellStatus(cell._1, cell._2).right.toOption.getOrElse(grid)
    }
    grid = grid.setAllHints(Axis.Vertical, generator.getRowHintFromPrologGrid(engine))
    grid = grid.setAllHints(Axis.Horizontal, generator.getColumnHintFromPrologGrid(engine))
    grid
  }
}


/** Factory for [[Grid]] instances with type parameter boolean.
  * I.e. factory for black&white grid
  */
object BooleanRandomGridFactory extends RandomGridFactoryTemplate[Boolean] {
  override protected def defaultStatus: Boolean = false
  override protected def generator: PrologGrid[Boolean] = PrologGrid()
  override protected implicit val gridTypeTag: ClassTag[Boolean] = implicitly[ClassTag[Boolean]]
}
