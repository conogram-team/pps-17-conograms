package game.singleplayer.model.grid.gridfactory

import alice.tuprolog.Theory
import utility.prolog.ScalaToProlog._

/** Provides methods to use a Prolog grid
  *
  * @tparam CellType the type of the grid (e.g. Boolean, Color)
  */
trait PrologGrid[CellType] {
  type Cells = Map[(Int, Int), CellType]
  type Hints = Map[Int, Seq[(Int, CellType)]]

  /** Produce an engine with a grid based on passed parameters */
  def produceGrid(row: Int, column: Int, fullCell: Int): Engine
  /** Extracts the cells from the Prolog Engine to a Map
    *
    * @param engine The Prolog engine which contains the grid
    */
  def getCellFromPrologGrid(engine: Engine): Cells
  /** Extracts the rows hints from the Prolog Engine to a Map
    *
    * @param engine the Prolog engine which contains the grid
    */
  def getRowHintFromPrologGrid(engine: Engine): Hints
  /** Extracts the columns hints from the Prolog Engine to a Map
    *
    * @param engine the Prolog engine which contains the grid
    */
  def getColumnHintFromPrologGrid(engine: Engine): Hints

}

/** Provides some general template methods implementation for [[PrologGrid]] */
trait GeneralPrologGrid[CellType] extends PrologGrid[CellType]{

  protected def nonogramsPrologFilepath: String = "/Nonograms.pl"
  protected def generatePrologGrid(goal: String): Engine = {
    val engine = makePrologEngine(new Theory(getClass.getResourceAsStream(nonogramsPrologFilepath)))
    solveWithSuccess(engine, goal)
    engine
  }

  protected def rowFunctor: String = "row_clue"
  override def getRowHintFromPrologGrid(engine: Engine): Hints =
    getHintFromPrologGrid(rowFunctor, engine)

  protected def columnFunctor: String = "column_clue"
  override def getColumnHintFromPrologGrid(engine: Engine): Hints =
    getHintFromPrologGrid(columnFunctor, engine)

  protected def getHintFromPrologGrid(functor: String, engine: Engine): Hints

}

private class BooleanPrologGrid extends GeneralPrologGrid[Boolean]{
  override type Cells = Map[(Int, Int), Boolean]
  override type Hints = Map[Int, Seq[(Int, Boolean)]]

  private val newGridFunctor = "new_grid"
  override def produceGrid(row: Int, column: Int, fullCell: Int): Engine = {
    val makeGridGoal = s"$newGridFunctor($row, $column, $fullCell)"
    generatePrologGrid(makeGridGoal)
  }

  private val rowVariable = "Row"
  private val columnVariable = "Column"
  private val blockQuery = s"block($rowVariable, $columnVariable)"
  override def getCellFromPrologGrid(engine: Engine): Cells =
    engine(blockQuery).map( cell =>
      (extractTerm(cell, rowVariable).toString.toInt,
        extractTerm(cell, columnVariable).toString.toInt) -> true).toMap

  private val indexVariable = "Index"
  private val hintVariable = "Hint"
  private val hintQueryBody = s"($indexVariable, $hintVariable)"
  protected override def getHintFromPrologGrid(functor: String, engine: Engine): Hints =
    engine(functor + hintQueryBody).map( clue =>
      extractTerm(clue, indexVariable).toString.toInt ->
        prologListToSeq(extractTerm(clue, hintVariable)).map(_.toInt).map( (_, true) )).toMap
}

/** Factory for [[PrologGrid]] */
object PrologGrid {
  /** Produces a [[PrologGrid]] of boolean, the engine will handle a black&white grid */
  def apply(): PrologGrid[Boolean] = new BooleanPrologGrid()
}