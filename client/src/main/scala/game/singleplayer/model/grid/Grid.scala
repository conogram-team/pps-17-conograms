package game.singleplayer.model.grid

import game.singleplayer.model.grid
import game.singleplayer.model.grid.Axis.Axis
import game.singleplayer.model.grid.EitherMessage.EitherMessage
import game.singleplayer.model.grid.Grid.{AxisHints, Hints, Matrix, Row}

import scala.reflect.ClassTag

/**
  * An enumeration whose values represent the grid axes.
  */
object Axis extends Enumeration {
  type Axis = Value
  val Vertical, Horizontal = Value
}


object EitherMessage extends Enumeration {
  type EitherMessage = Value
  val PositionNotFound: Value = Value("Position not found")
}


/**
  * An trait that define the contract of the Grid data type.
  *
  * @tparam Status any data type with which to populate the grid.
  */
sealed trait Grid[Status] {

  /**
    * A method that returns the size of the grid.
    *
    * @return the size of the grid as a pair (number of rows, number of columns).
    */
  def getSize: (Int, Int)


  /**
    * A method that returns the number of full cells in the grid.
    *
    * @return the number of full cells.
    */
  def getNumberOfFullCells: Int


  /**
    * A method that returns the state of a cell, taken in input its position within the grid.
    *
    * @param position identifies the cell of interest as a pair (row number, column number).
    * @return the contents of the cell, known as the state.
    */
  def getCellStatusIn(position: (Int, Int)): Either[EitherMessage, Status]


  /**
    * A method that sets the status of a cell given its position within the grid.
    *
    * @param position     identifies the cell of interest as a pair (row number, column number).
    * @param futureStatus the new content to be inserted in the cell.
    */
  def setCellStatus(position: (Int, Int), futureStatus: Status): Either[EitherMessage, Grid[Status]]


  /**
    * A method that returns hints for a given row or column, based on the input axis.
    *
    * @param axis     grid axis, either vertical or horizontal.
    * @param position identifies the row or column, based on the input axis, of interest.
    * @return the hints for a given row or column.
    */
  def getHints(axis: Axis, position: Int): Hints[Status]


  /**
    * A method for setting the hints for a given row or column, based on the axis and position of the cell that are given as input.
    *
    * @param axis     grid axis, either vertical or horizontal.
    * @param position identifies the row or column, based on the input axis, of interest.
    * @param hints    the hints to set for a given row or column, based on the input axis.
    */
  def setHints(axis: Axis, position: Int, hints: Hints[Status]): Grid[Status]


  /**
    * A method that returns all the hints for a given grid axis.
    *
    * @param axis grid axis, either vertical or horizontal.
    * @return all hints for a given axis.
    */
  def getAllHints(axis: Axis): AxisHints[Status]


  /**
    * A method for setting all the hints for a given grid axis.
    *
    * @param axis     grid axis, either vertical or horizontal.
    * @param allHints all hints to set for a given axis.
    */
  def setAllHints(axis: Axis, allHints: AxisHints[Status]): Grid[Status]

}


/**
  * A concrete class that implements, in a generic way, the interface of the Grid data type.
  *
  * @constructor create a new grid with a matrix ad-hoc
  * @param matrix a completed matrix to be set in the grid.
  * @tparam Status any data type with which to populate the grid.
  */
private case class DefaultGrid[Status](matrix: Matrix[Status],
                                       verticalAxisHints: AxisHints[Status],
                                       horizontalAxisHints: AxisHints[Status]) extends Grid[Status] {


  /**
    * @see [[grid.Grid]]
    * @return the size of the grid as a pair (number of rows, number of columns).
    */
  override def getSize: (Int, Int) = (this.matrix.length, this.matrix.head.length)


  /**
    * @see [[grid.Grid]]
    * @return the number of full cells.
    */
  override def getNumberOfFullCells: Int =
    if (this.getAllHints(Axis.Vertical).nonEmpty) {
      numberOfFullCells(Axis.Vertical)
    } else {
      numberOfFullCells(Axis.Horizontal)
    }


  /**
    * @see [[grid.Grid]]
    * @param position identifies the cell of interest as a pair (row number, column number).
    * @return the contents of the cell, known as the state.
    */
  override def getCellStatusIn(position: (Int, Int)): Either[EitherMessage, Status] =
    if (isValidPosition(position)) {
      Right(matrix(position._1)(position._2))
    } else {
      Left(EitherMessage.PositionNotFound)
    }


  /**
    * @see [[grid.Grid]]
    * @param position     identifies the cell of interest as a pair (row number, column number).
    * @param futureStatus the new content to be inserted in the cell.
    */
  override def setCellStatus(position: (Int, Int), futureStatus: Status): Either[EitherMessage, Grid[Status]] =
    if (isValidPosition(position)) {
      if (cellStatusMustBeUpdated(position, futureStatus)) {
        val updateRow = matrix(position._1).updated(position._2, futureStatus)
        val updatedMatrix = matrix.updated(position._1, updateRow)

        Right(Grid(updatedMatrix, verticalAxisHints, horizontalAxisHints))
      } else {
        Right(this)
      }
    } else {
      Left(EitherMessage.PositionNotFound)
    }

  /**
    * @see [[grid.Grid]]
    * @param axis     grid axis, either vertical or horizontal.
    * @param position identifies the row or column, based on the input axis, of interest.
    * @return the hints for a given row or column.
    */
  override def getHints(axis: Axis, position: Int): Hints[Status] = axis match {
    case Axis.Vertical => this.verticalAxisHints.getOrElse(position, Seq())
    case Axis.Horizontal => this.horizontalAxisHints.getOrElse(position, Seq())
  }


  /**
    * @see [[grid.Grid]]
    * @param axis     grid axis, either vertical or horizontal.
    * @param position identifies the row or column, based on the input axis, of interest.
    * @param hints    the hints to set for a given row or column, based on the input axis.
    */
  override def setHints(axis: Axis, position: Int, hints: Hints[Status]): Grid[Status] = axis match {
    case Axis.Vertical => Grid(matrix, verticalAxisHints + (position -> hints), horizontalAxisHints)
    case Axis.Horizontal => Grid(matrix, verticalAxisHints, horizontalAxisHints + (position -> hints))
  }


  /**
    * @see [[grid.Grid]]
    * @param axis grid axis, either vertical or horizontal.
    * @return all hints for a given axis.
    */
  override def getAllHints(axis: Axis): AxisHints[Status] = axis match {
    case Axis.Vertical => this.verticalAxisHints
    case Axis.Horizontal => this.horizontalAxisHints
  }


  /**
    * @see [[grid.Grid]]
    * @param axis     grid axis, either vertical or horizontal.
    * @param allHints all hints to set for a given axis.
    */
  override def setAllHints(axis: Axis, allHints: AxisHints[Status]): Grid[Status] = axis match {
    case Axis.Vertical => Grid(matrix, allHints, horizontalAxisHints)
    case Axis.Horizontal => Grid(matrix, verticalAxisHints, allHints)
  }


  private def numberOfFullCells(axis: Axis) =
    this.getAllHints(axis)
      .flatMap(rowHints => rowHints._2
        .map(hints => hints._1)).sum


  private def isValidPosition(position: (Int, Int)): Boolean =
    position._1 < this.matrix.length & position._2 < this.matrix(position._1).length


  private def cellStatusMustBeUpdated(position: (Int, Int), futureStatus: Status): Boolean =
    this.matrix(position._1)(position._2) != futureStatus

}


/**
  * Companion object to create a row of the matrix
  */
object Row {
  def apply[Status](status: Status*): Row[Status] = Seq(status: _*)
}


/**
  * Companion object to create all hints on an axis
  */
object AxisHints {

  def apply[Status](axisHints: (Int, Seq[Int])*)(implicit defaultStatus: Status): AxisHints[Status] =
    Map(axisHints.map(hints => (hints._1, hints._2.map(index => index -> defaultStatus))):_*)

  def apply[Status](axisHints: (Int, Hints[Status])*): AxisHints[Status] = Map(axisHints: _*)

  def apply[Status](): AxisHints[Status] = Map.empty[Int, Hints[Status]]

}


/**
  * Companion object to create a matrix
  */
object Matrix {
  def apply[Status](rows: Row[Status]*): Matrix[Status] = Seq(rows: _*)
}


/**
  * The companion object of the grid useful to prevent the client from using the concrete class.
  */
object Grid {

  type Row[Status] = Seq[Status]
  type Matrix[Status] = Seq[Row[Status]]
  type Hints[Status] = Seq[(Int, Status)]
  type AxisHints[Status] = Map[Int, Hints[Status]]

  def apply[Status](matrix: Matrix[Status],
                    verticalAxisHints: AxisHints[Status] = AxisHints(),
                    horizontalAxisHints: AxisHints[Status] = AxisHints()): Grid[Status] =
    new DefaultGrid[Status](matrix, verticalAxisHints, horizontalAxisHints)


  def apply[Status: ClassTag](numberOfRows: Int, numberOfColumns: Int)(implicit defaultStatus: Status): Grid[Status] = {
    val matrix: Matrix[Status] = Seq.fill[Status](numberOfRows, numberOfColumns)(defaultStatus)
    new DefaultGrid[Status](matrix, AxisHints(), AxisHints())
  }

}
