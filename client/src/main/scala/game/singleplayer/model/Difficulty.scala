package game.singleplayer.model

import scala.language.implicitConversions

/** The difficulty of the game as Enumeration
  *  Expressed through Low, Medium, High
  *  The values of the enum provides a method fullCellsOf that
  *  compute the number of full cells for the given dimensions
  */
object Difficulty extends Enumeration {
  /** Alias to use Difficulty as Enumeration type */
  type Difficulty = Value

  protected case class Val(ratio: Double) extends super.Val {
    def fullCellsOf(rows: Int, columns: Int): Int = (rows * columns * ratio).toInt
  }

  /** Needed to provide a rich Enumeration with utility method
    *
    * @param x the Difficulty Enumeration instance
    */
  implicit def valueToDifficultyVal(x: Value): Val = x.asInstanceOf[Val]

  /** High level of difficulty, density of 30% of cells */
  val High = Val(0.3)
  /** Medium level of difficulty, density of 50% of cells */
  val Medium = Val(0.5)
  /** Low level of difficulty, density of 70% of cells */
  val Low = Val(0.7)
}
