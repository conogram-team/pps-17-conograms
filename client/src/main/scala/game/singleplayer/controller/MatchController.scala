package game.singleplayer.controller

import game.multiplayer.view.MultiplayerMenuGUI
import game.singleplayer.model.StatusOfMatch.StatusOfMatch
import game.singleplayer.model.grid.Grid
import game.singleplayer.model.{MatchModel, Player, StatusOfMatch}

import game.singleplayer.view.ConogramView

import scala.language.postfixOps
import scala.swing.Color

/**
  * Trait that represent the controller, according to the MVC pattern, for the match logic.
  * @tparam T the type of the status of a cell.
  */
trait MatchController[T] extends PlayerController[T] with GridController[T] {
  private val NumberOfLivesToLoss = 0

  protected var _matchView: Option[ConogramView] = None

  override protected def matchModel: MatchModel[T]

  /**
    * When a cell is selected some actions are triggered.
    * The number of available lives is decremented, for example, if the selection is wrong or, on the other hand,
    * if the selection let the player win then the state of match changes.
    *
    * @param position       The position of the cell selected by the user.
    * @param supposedStatus The supposed status of the cell selected by the user.
    */
  def selectCellIn(position: (Int, Int), supposedStatus: T, player: Player = localPlayer.get): Unit = {
    def isTheSelectionRight = matchModel.checkCellStatusIn(position, supposedStatus)

    if (!getSelectedCells(player).contains(position)) {
      player.addSelectedCellIn(position)

      if (!isTheSelectionRight) {
        player.decrementAvailableLives()
      } else {
        player.incrementRightSelectionNumber()
      }

      setNewStatusOfMatchFor(player)
    }
  }

  /**
    * @return The whole cells selected during the match.
    */
  def getSelectedCells(player: Player = localPlayer.get): Map[(Int, Int), (T, Color)] = {
    def getRightCellStatusIn(position: (Int, Int)) = matchModel.getCellStatusIn(position)

    def getSelectionColor(point: (Int, Int)): Color = {
      if (matchModel.getCellStatusIn(point).get == getRightCellStatusIn(point).get) {
        matchModel.getLocalPlayerColors._1
      } else {
        matchModel.getLocalPlayerColors._2
      }
    }

    localPlayer.get.getSelectedCells map
      (cell => cell -> (matchModel.getCellStatusIn(cell).get, getSelectionColor(cell))) toMap
  }

  /**
    * Setter for the status of the match, which, for instance, could change after a cell selection.
    * @param player The player whose status changes.
    */
  def setNewStatusOfMatchFor(player: Player = localPlayer.get): Unit =
    if (getWinners.contains(player)) {
      matchModel.statusOfMatch_=(StatusOfMatch.Win)
    } else if (player.getAvailableLives == NumberOfLivesToLoss) {
      matchModel.statusOfMatch_=(StatusOfMatch.Loss)
    }

  /**
    * @return Returns the state of a match which could be, for instance, running/win/loss.
    */
  def statusOfMatch: StatusOfMatch = matchModel.statusOfMatch

  /**
    * @return the player who won the match
    */
  def getWinners: Set[Player] = if (hasThePlayerWon) Set(localPlayer.get) else Set()

  /**
    * Setter for the match view.
    *
    * @param matchView The match view.
    */
  def matchView_=(matchView: ConogramView): Unit = _matchView = Some(matchView)

  /**
    * Opens a new MenuView to start a new match.
    */
  def openMenuView(): Unit = {
    new MultiplayerMenuGUI().openView()
  }

  /**
    * @return Returns whether the player (the loacal player, by default) has won the match.
    */
  protected def hasThePlayerWon: Boolean = matchModel.hasThePlayerWon()
}

/**
  * Companion object of the MatchController trait for the single player match.
  */
object SinglePlayerMatchController {
  def apply[T](grid: Grid[T]): MatchController[T] = new MatchController[T] {
    override protected val matchModel: MatchModel[T] = MatchModel(grid)
  }
}
