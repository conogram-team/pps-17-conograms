package game.singleplayer.controller

import account.Account
import game.singleplayer.controller
import game.singleplayer.model.grid.Grid
import game.singleplayer.model.grid.gridfactory.BooleanRandomGridFactory
import game.singleplayer.view.builder.MatchViewBuilder
import game.singleplayer.view.{MatchViewNonogram, MenuView, View}

import scala.concurrent.ExecutionContext

/**
  * A trait that takes care of instantiating a match
  */
trait MenuController {

  protected val view: View

  /**
    * A method that takes care of instantiating a match
    *
    * @param numberOfRows number of rows that the grid must have
    * @param numberOfColumns number of columns that the grid must have
    * @param fullCellsNumber number of full cells that the grid must have
    */
  def startMatch(numberOfRows: Int,
                 numberOfColumns: Int,
                 fullCellsNumber: Int): Unit = view.close()

  /**
    * A method to handle the end of a match
    */
  def endMatch(): Unit = view.display()


  protected def runInSafeContext(task: => Unit): Unit = {
    ExecutionContext.global.execute(new Runnable {
      override def run(): Unit = task
    })
  }

  protected def setupController[T](grid: Grid[T]): MatchController[T] = {
    val controller: MatchController[T] = SinglePlayerMatchController(grid)
    controller.localPlayer_=(game.singleplayer.model.Player(Account.currentUser.get))
    controller
  }


  protected def startMatchView[T](controller: MatchController[T], viewSize: (Int, Int))(implicit defaultStatus: T): Unit = {
    val matchViewBuilder= new MatchViewBuilder[T](viewSize._1, viewSize._2, defaultStatus).build()
    val view = new MatchViewNonogram[T](matchViewBuilder.get, controller)
    controller.matchView_=(view)
  }

}


/**
  * An object to creating a boolean match
  */
object MenuController {

  def apply(menuView: MenuView): MenuController = new MenuController {

    override protected val view: View = menuView

    /**
      * @see [[MenuController]]
      * @param numberOfRows number of rows that the grid must have
      * @param numberOfColumns number of columns that the grid must have
      * @param fullCellsNumber number of full cells that the grid must have
      */
    override def startMatch(numberOfRows: Int, numberOfColumns: Int, fullCellsNumber: Int): Unit = {
      runInSafeContext {
        implicit val defaultStatus: Boolean = true
        val grid: Grid[Boolean] = BooleanRandomGridFactory(numberOfRows, numberOfColumns, fullCellsNumber)
        val controller: MatchController[Boolean] = setupController(grid)
        startMatchView(controller, grid.getSize)
      }

      super.startMatch(numberOfRows, numberOfColumns, fullCellsNumber)
    }

  }

}