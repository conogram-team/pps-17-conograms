package game.singleplayer.controller

import game.singleplayer.model.{MatchModel, Player}

trait PlayerController[T] {

  protected def matchModel: MatchModel[T]

  /**
    * @return The local player whithin the match.
    */
  def localPlayer: Option[Player] = matchModel.localPlayer

  /**
    * Setter for the local player whithin the match.
    * @param player the local player whithin the match.
    */
  def localPlayer_=(player: Player): Unit = matchModel.localPlayer_=(player)

  /**
    * @return Returns the number of remaining lives for a player
    */
  def getRemainingLives: Int = matchModel.localPlayer.get.getAvailableLives
}
