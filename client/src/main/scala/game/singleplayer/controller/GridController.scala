package game.singleplayer.controller

import game.singleplayer.model.MatchModel
import game.singleplayer.model.grid.Axis

trait GridController[T] {

  protected def matchModel: MatchModel[T]

  /**
    * @return Returns the whole hints useful for the player to suppose the status of a cell within the grid.
    */
  def getHints: (Map[Int, Seq[(Int, T)]], Map[Int, Seq[(Int, T)]]) =
    (matchModel.getGridHints(Axis.Vertical), matchModel.getGridHints(Axis.Horizontal))

  /**
    * @return Returns the max number of hints within the match grid.
    */
  def getMaxHintsNumber: Int = matchModel.getMaxHintsNumber

  /**
    * @return Returns the size of the grid whithin the match.
    */
  def getGridSize: (Int, Int) = matchModel.getGridSize

}
