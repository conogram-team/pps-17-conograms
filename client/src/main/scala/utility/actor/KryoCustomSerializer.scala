/** *****************************************************************************
  * Copyright 2012 Roman Levenstein
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  * Code from https://github.com/Random-Liu/simplified-akka-kryo-serialization/
  * blob/master/src/main/scala/com/romix/akka/serialization/kryo/KryoSerializer.scala
  * *****************************************************************************/
package utility.actor

import akka.serialization._
import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.{Input, Output}

/**
  * Custom Serializer that uses the Kryo serializer framework
  * @param kryo The Kryo serializer that map classes into serializers.
  * @param bufferSize The size of the data buffer.
  * @param maxBufferSize The max size of a strict buffer.
  */
class KryoCustomSerializer(val kryo: Kryo, val bufferSize: Int, val maxBufferSize: Int) extends Serializer {
  private val buf = new Output(bufferSize, maxBufferSize)
  private val SerializerIdentifier = 342896830

  override def identifier: Int = SerializerIdentifier
  override def includeManifest: Boolean = true

  override def toBinary(obj: AnyRef): Array[Byte] = {
    val buffer = getBuffer
    try {
      kryo.writeClassAndObject(buffer, obj)
      buffer.toBytes
    } finally
      releaseBuffer(buffer)
  }

  override def fromBinary(bytes: Array[Byte], clazz: Option[Class[_]]): AnyRef = {
    kryo.readClassAndObject(new Input(bytes))
  }

  private def getBuffer = buf
  private def releaseBuffer(buffer: Output): Unit = {
    buffer.clear()
  }
}
