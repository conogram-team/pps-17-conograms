package utility.grid

import game.singleplayer.model.grid.Grid

import scala.annotation.tailrec

/**
  * Grid serializer to convert the grid to different string formats
  */
object GridSerializer {

  private val firstIndex = 0

  /**
    * A method that converts the grid into a string of the type
    * [Number of rows: value, number of columns: value, number of full cells: value]
    *
    * @param grid to convert in string
    * @tparam T the type of the grid
    * @return a grid in string format
    */
  def toString[T](grid: Grid[T]): String = s"[Number of rows: ${grid.getSize._1}" concat
    s", number of columns: ${grid.getSize._2}" concat
    s", number of full cells: ${grid.getNumberOfFullCells}]"

  /**
    * A method that converts the grid into a string of the type
    * (row: value, column: value, status: value)
    *
    * @param grid to convert in string
    * @tparam T the type of the grid
    * @return a grid in string format
    */
  def toStringDetailed[T](grid: Grid[T]): String = this.convertToString(grid)((row: Int, column: Int) =>
    grid.getCellStatusIn(row, column) match {
      case Right(status) => s"(row: $row, column: $column) => status: $status\n"
      case Left(message) => s"Failed: $message"
    })

  /**
    * A method that converts the grid into a string in a matrix format
    *
    * @param grid to convert in string
    * @tparam T the type of the grid
    * @return a grid in string format
    */
  def toStringAsMatrix[T](grid: Grid[T]): String = this.convertToString(grid)((row: Int, column: Int) =>
    (column, grid.getCellStatusIn(row, column)) match {
      case (`firstIndex`, Right(status)) => s"\n$status\t"
      case (`firstIndex`, Left(message)) => s"\n$message\t"
      case (_, Right(status)) => s"$status\t"
      case (_, Left(message)) => s"$message\t"
  })

  private def convertToString[T](grid: Grid[T])(function: (Int, Int) => String): String = {


    @tailrec
    def convertRecursively(position: (Int, Int), size: (Int, Int), stringConverted: String): String = position match {
      case (size._1, `firstIndex`) => stringConverted
      case (row, column) if column == size._2 - 1 =>
        convertRecursively((row + 1, `firstIndex`), size, stringConverted concat function(row, column))
      case (row, column) =>
        convertRecursively((position._1, position._2 + 1), size, stringConverted concat function(row, column))
    }

    convertRecursively(position = (firstIndex,firstIndex), grid.getSize, "")
  }
}
