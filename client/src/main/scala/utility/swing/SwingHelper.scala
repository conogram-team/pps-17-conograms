package utility.swing

import javax.swing.SwingUtilities

import scala.language.implicitConversions
//noinspection ScalaStyle
import java.awt.event.{ActionEvent, ActionListener}

import javax.swing.{JComponent, JFrame}

import scala.swing.Dimension

/** Implicit conversions and helpers for Swing */
object SwingHelper {
  /** Convert a function that consume an event to an java.awt.event.ActionListener attachable to a Swing component */
  implicit def functionToActionListener(handler: ActionEvent => Unit): ActionListener = new ActionListener {
    override def actionPerformed(event: ActionEvent): Unit = handler(event)
  }
  /** Convert a function without parameters to an java.awt.event.ActionListener attachable to a Swing component */
  implicit def unitToActionListener(handler: => Unit): ActionListener = new ActionListener {
    override def actionPerformed(event: ActionEvent): Unit = handler
  }
  /** Convert couple of int to Dimension */
  implicit def tupleToDimension(dimension: (Int, Int)): Dimension = new Dimension(dimension._1, dimension._2)

  /** Add a variety of utility method to JFrame */
  implicit class RichFrame(frame: JFrame) {
    /** Add all the components to the frame*/
    def addAllComponent(components: JComponent*): Unit = {
      components.foreach(frame.add(_))
    }
  }

  /** Convert a unit function to a runnable */
  implicit def runnable(action: => Unit): Runnable = new Runnable() { def run(): Unit = action }

  /** Thread safe invoking of unit function which use Swing method
    *  invoke on EDT only if the thread isn't the EDT
    */
  def safeInvokeAndWait(action: => Unit): Unit = {
    if(SwingUtilities.isEventDispatchThread){
      action
    } else{
      SwingUtilities.invokeAndWait( action )
    }
  }
}
