% assert_once(+Fact).
% Assert a fact only if it isn't already in the theory
assert_once(Fact) :- ( Fact, ! ; assertz(Fact) ).

% sequence(?Start, ?End, ?Sequence).
% Correlates the complete list of ordered numbers that is
% starting with Start and ending in End
sequence([], [], []).
sequence(Start, Start, [Start]).
sequence(Start, End, [Start | Follow]) :-
    Next is Start + 1,
    sequence(Next, End, Follow).

% subset(?List1, ?List2).
% Correlates List1 with it's subset.
subset([], []).
subset([Element|Tail], [Element|SubTail]):-
  subset(Tail, SubTail).
subset([_|Tail], SubTail):-
  subset(Tail, SubTail).

% remove_list(+List, +Remove, -Result).
% Removes the elements in Remove from List.
remove_list([], _, []).
remove_list([Head|Tail], Remove, Result):-
    member(Head, Remove), !,
    remove_list(Tail, Remove, Result).
remove_list([Head|Tail], Remove, [Head|Result]):-
    remove_list(Tail, Remove, Result).