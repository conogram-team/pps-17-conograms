:- include("client/src/main/prolog/Utility.pl").

% new_grid(+Row, +Column, +Block).
% Row and Column define the dimension of the new grid,
% Block is the number of Full Blocks that are in the grid.
% Retract all the fact about previous grid and
% assert a new random grid, finally compute the grid clues.
new_grid(Row, Column, Block) :-
    retractall(block(_, _)),
    retractall(grid_row(_)),
    retractall(grid_column(_)),
    retractall(grid_block(_)),
    assertz(grid_row(Row)),
    assertz(grid_column(Column)),
    assertz(grid_block(Block)),
    populate_grid(Row, Column),
    produce_row_clues(0),
    produce_column_clues(0).

% populate_grid(+Row, +Column).
% Populate the new grid with random blocks until
% total blocks satisfy grid_block(Total).
populate_grid(Row, Column) :-
    rand_int(Row, RandomRow),
    rand_int(Column, RandomColumn),
    assert_once(block(RandomRow, RandomColumn)),
    (blocks(_, _, Total), grid_block(Total), ! ; populate_grid(Row, Column)).

% blocks(?Row, ?Column, ?Count).
% Correlates Row and Column index with the number of blocks with those index.
% E.g. blocks(3, _, Count). Count is the number of blocks in row #3.
blocks(Row, Column, Count) :-
    findall(_, block(Row, Column), Blocks),
    length(Blocks, Count).

% produce_row_clues(+Index).
% Produce the clues for rows,
% starting from Index until the end of the grid.
produce_row_clues(Index) :-
    grid_row(Index), !.
produce_row_clues(Index) :-
    findall(Column, block(Index, Column), RowBlocks),
    start_produce_clues(RowBlocks, RowClues), !,
    assertz(row_clue(Index, RowClues)),
    Next is Index + 1,
    produce_row_clues(Next).

% produce_column_clues(+Index).
% Produce the clues for columns,
% starting from Index until the end of the grid.
produce_column_clues(Index) :-
    grid_column(Index), !.
produce_column_clues(Index) :-
    findall(Row, block(Row, Index), ColumnBlocks),
    start_produce_clues(ColumnBlocks, ColumnClues), !,
    assertz(column_clue(Index, ColumnClues)),
    Next is Index + 1,
    produce_column_clues(Next).

% start_produce_clues(+Blocks, -Clues).
% Blocks is the sequence of blocks in the same row or column,
% Clues is the list of clues related to the sequence.
start_produce_clues(Blocks, Clues) :-
    quicksort(Blocks, '<', SortedBlocks),
    process_clue(SortedBlocks, [], Clues).

% process_clue(+ToProcess, +OldClues, - Clues).
% Used internally by 'start_produce_clues'
process_clue([], [], [0]).
process_clue([], Clues, Clues).
process_clue(ToProcess, OldClues, NewClues) :-
    subset(ToProcess, Subset),
    sequence(_, _, Subset),
    remove_list(ToProcess, Subset, Remain),
    length(Subset, Count),
    append(OldClues, [Count], NextClues),
    process_clue(Remain, NextClues, NewClues).

