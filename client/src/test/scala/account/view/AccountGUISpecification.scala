package account.view

import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen, OneInstancePerTest}

class AccountGUISpecification extends FeatureSpec
  with GivenWhenThen with BeforeAndAfter with MockFactory with OneInstancePerTest {

  val username = "Alice"
  val password = "password"
  val error = "Connection error"

  val viewObserver = stub[AccountViewObserver]
  var gui: AccountGUI = _

  (viewObserver.loginEvent _).when(_: String, _: String)

  before {
    gui = new AccountGUI()
    gui.addAccountViewObserver(viewObserver)
  }

  feature("A view is provided to the user for login and signup") {

    info("As a User")
    info("I want a view to log in to the system")
    info("In order to play and interact with other players")

    scenario("A user click the login button") {
      Given("an account view with somethings in username and password field")
      gui.usernameField.setText(username)
      gui.passwordField.setText(password)
      When("the user click the login button")
      gui.loginButton.doClick()
      Then("the observers are notified with the content of username and password field")
      (viewObserver.loginEvent _).verify(username, password)
    }

    scenario("A user click the signup button") {
      Given("an account view with somethings in username and password field")
      gui.usernameField.setText(username)
      gui.passwordField.setText(password)
      When("the user click the signup button")
      gui.signupButton.doClick()
      Then("the observers are notified with the content of username and password field")
      (viewObserver.signupEvent _).verify(username, password)
    }

    scenario("An operation fails") {
      When("the view is informed that an error occur")
      gui.informError(error)
      Then("the error is shown to the user")
      assert(gui.inform.getText == error)
    }

    scenario("The view must be shown") {
      When("the view is needed")
      gui.display()
      Then("the view is shown")
      assert(gui.isVisible)
    }

    scenario("The view is disposed") {
      When("the view isn't needed anymore")
      gui.close()
      Then("the view is closed")
      assert(!gui.isDisplayable)
    }
  }
}
