package account.model

import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class ConogramUserSpecification extends FeatureSpec with GivenWhenThen with BeforeAndAfter {

  val username = "Alice"
  val token = "abc"
  val location = "localhost"

  var user: ConogramUser[String] = _

  before {
    user = ConogramUser(username)
  }

  feature("The user is represented into the system") {

    info("As a Programmer")
    info("I want to represent users in the system")
    info("So that I can track their info")

    scenario("A user is created") {
      Given("a user")
      When("is instantiated")
      Then("the user has a username")
      assert(user.username == username)
    }

    scenario("A user has a token") {
      Given("a user")
      When("a token is set")
      user = user.withToken(Some(token))
      Then("the user has the token")
      assert(user.token.get == token)
    }

    scenario("A user can have a location needed for peer to peer communication") {
      Given("a user")
      When("an IP is set")
      user = user.withLocation(Some(location))
      Then("the user has the IP")
      assert(user.location.get == location)
    }

  }
}
