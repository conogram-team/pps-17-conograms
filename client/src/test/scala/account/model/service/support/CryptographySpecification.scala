package account.model.service.support

import org.scalatest.{FeatureSpec, GivenWhenThen}

class CryptographySpecification extends FeatureSpec with GivenWhenThen {

  val message = "message"
  val computedMessageHashSHA256 = "ab530a13e45914982b79f9b7e3fba994cfd1f3fb22f71cea1afbf02b460c6d1d"
  val computedMessageHashDefaultEncode = computedMessageHashSHA256

  feature("Cryptography utilities") {

    info("As a Programmer")
    info("I want to use cryptography")
    info("In order to enchants the system security")

    scenario("SHA-256 hashing") {
      Given("a message")
      When("it is encoded with SHA-256")
      val hashedMessage = Cryptography.encodeSHA256(message)
      Then("the hash is correct")
      assert(hashedMessage == computedMessageHashSHA256)
    }

    scenario("The default encoding is the agreed one in server protocol") {
      Given("a message")
      When("it is encoded with the default encoding")
      val hashedMessage = Cryptography.defaultUserApiEncode(message)
      Then("the hash is correct")
      assert(hashedMessage == computedMessageHashDefaultEncode)
    }
  }
}
