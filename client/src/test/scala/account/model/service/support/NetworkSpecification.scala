package account.model.service.support

import java.net.InetAddress

import org.scalatest.{FeatureSpec, GivenWhenThen}

class NetworkSpecification extends FeatureSpec with GivenWhenThen {

  import Network._

  val ipPortConcatenation = ":"

  feature("Network utilities") {

    info("As a Programmer")
    info("I need some network utilities")

    scenario("verifying a string that contains a lan addresses") {
      Given("a lan address")
      val ip = "192.168.0.1"
      When("it is verified")
      Then("it is a lan ip")
      assert(isLanIP(ip))
    }

    scenario("verifying a string that doesn't contains a lan addresses") {
      Given("an address not in lan range")
      val ip = "8.8.8.8"
      When("it is verified")
      Then("it isn't a lan ip")
      assert(!isLanIP(ip))
    }

    scenario("lan address is needed") {
      When("the lan IP is requested")
      val ip = localAddress
      Then("the string contains a valid lan ip, or at least the loopback address")
      assert(ip == InetAddress.getLocalHost.getHostAddress || isLanIP(ip))
    }

    scenario("the actor system for the application is bound") {
      When("the actor system is bound")
      bindRemoteSystem(localAddress, 0)
      Then("an actor system is created")
      assert(applicationActorSystem.nonEmpty)
      And("an address is assigned to it")
      assert(userBoundAddress.nonEmpty)
      And("a port is assigned to it")
      assert(userBoundPort.nonEmpty)
    }

    scenario("the complete address of the client is taken") {
      When("the actor system is bound")
      bindRemoteSystem(localAddress, 0)
      Then("the complete address is the concatenation of address and port")
      assert(userBoundCompleteAddress.get == userBoundAddress.get + ipPortConcatenation + userBoundPort.get)
    }
  }
}
