package account.model.service

import org.scalatest.{FeatureSpec, GivenWhenThen}
import pdi.jwt.algorithms.JwtHmacAlgorithm
import pdi.jwt.{Jwt, JwtAlgorithm}

import support.Cryptography._

class TokenManagerJWTSpecification extends FeatureSpec with GivenWhenThen{

  val username = "Alice"
  val password = "password"
  val hashedPassword = defaultUserApiEncode(password)
  val jwtAlgorithm: JwtHmacAlgorithm = JwtAlgorithm.HS256
  val decodeAlgorithm = Seq(jwtAlgorithm)
  val tokenManagerJWT: TokenManager[String] = TokenManagerJWT(jwtAlgorithm)
  val tokenManagerJWTwithHash: TokenManager[String] = TokenManagerJWT.withHash(jwtAlgorithm)

  feature("A token is used for authentication") {

    info("As a Programmer")
    info("I want to use a Bearer authentication scheme")
    info("In order to presenting claims securely between two parties")

    scenario("A token is generated from username and password") {
      Given("a username and password")
      When("encode from them")
      val token = tokenManagerJWT.buildToken(username, password)
      Then("the result token can be validate knowing the password")
      Jwt.isValid(token, password, decodeAlgorithm)
      And("it contains the username")
      assert(Jwt.decode(token, password, decodeAlgorithm).get.contains(username))
    }

    scenario("A token is generated from username and password which is hashed before") {
      Given("a username and password")
      When("encode from them")
      val token = tokenManagerJWTwithHash.buildToken(username, password)
      Then("the result token can be validate knowing the hash of the password")
      Jwt.isValid(token, hashedPassword, decodeAlgorithm)
      And("it contains the username")
      assert(Jwt.decode(token, hashedPassword, decodeAlgorithm).get.contains(username))
    }
  }
}