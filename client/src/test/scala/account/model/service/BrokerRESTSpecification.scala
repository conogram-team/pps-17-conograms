package account.model.service

import rest.user.api.UserApi
import rest.user.model.User
import org.scalamock.scalatest.MockFactory

import scala.language.postfixOps
import org.scalatest.{FeatureSpec, GivenWhenThen, OneInstancePerTest}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.Try

import account.model.service.support.Cryptography._
import account.model.service.support.Network._

import scala.concurrent.ExecutionContext.Implicits.global

class BrokerRESTSpecification extends FeatureSpec with GivenWhenThen with MockFactory with OneInstancePerTest {

  val username = "Alice"
  val invalidUsername = "invalid"
  val throwException = "exception"
  val password = "password"
  val hashedPassword = defaultUserApiEncode(password)

  val responseMessageOK = ""
  val responseMessageErrorDetail = "Wrong fields"
  val responseMessageError = "{\"name\":\"BadRequest\",\"detail\":\"" + responseMessageErrorDetail + "\"}"
  val unknownFormatError = "Some error"
  val generalErrorMessage = "Some error occurred"

  val token = "token"
  val authenticationHeader = "Authorization"


  val userToRegister: User = User(username, Some(hashedPassword))
  val invalidUserToRegister: User = User(invalidUsername, Some(hashedPassword))
  val invalidUserToRegisterWithUnknownError: User = User(unknownFormatError, Some(hashedPassword))
  val throwUserToRegister: User = User(throwException, Some(hashedPassword))

  val userToLogin: User = User(username, ip = userBoundCompleteAddress)
  val invalidUserToLogin: User = User(invalidUsername, ip = userBoundCompleteAddress)
  val invalidUserToLoginWithUnknownError: User = User(unknownFormatError, ip = userBoundCompleteAddress)
  val throwUserToLogin: User = User(throwException, ip = userBoundCompleteAddress)


  class MockableUserApi() extends UserApi
  val userApi = stub[MockableUserApi]
  (userApi.createUserAsync _).when(userToRegister).returns(Future(responseMessageOK))
  (userApi.createUserAsync _).when(invalidUserToRegister).returns(Future(responseMessageError))
  (userApi.createUserAsync _).when(invalidUserToRegisterWithUnknownError).returns(Future(unknownFormatError))
  (userApi.createUserAsync _).when(throwUserToRegister).returns(Future.failed(new Exception(throwException)))

  (userApi.loginUserAsync _).when(userToLogin).returns(Future(responseMessageOK))
  (userApi.loginUserAsync _).when(invalidUserToLogin).returns(Future(responseMessageError))
  (userApi.loginUserAsync _).when(invalidUserToLoginWithUnknownError).returns(Future(unknownFormatError))
  (userApi.loginUserAsync _).when(throwUserToLogin).returns(Future.failed(new Exception(throwException)))

  (userApi.addHeader _).when(authenticationHeader, token).returns(_)

  val accountServiceBroker: AccountServiceBroker = BrokerREST(userApi)
  bindRemoteSystem(defaultAddress, 0)

  feature("A broker provide interaction with the server") {

    info("As a Programmer")
    info("I want to exploit the login and registration service offered by a server")
    info("In order to provide login and registration feature")

    scenario("A signup request is sent to the server") {
      Given("a valid username and password")
      When("the request is sent to the server")
      val response = blockingSignup(username, password)
      Then("the response asserts success")
      assert(response.get._1)
      And("it contains the expected message")
      assert(response.get._2 == responseMessageOK)
    }

    scenario("Before a login request, the user address is bound") {
      Given("a valid request")
      When("the request is performed")
      blockingLogin(username, token)
      Then("the user has a unique address")
      assert(userBoundCompleteAddress.nonEmpty)
    }

    scenario("A login request is sent to the server") {
      Given("a valid request")
      When("the request is sent to the server")
      val response = blockingLogin(username, token)
      Then("the response asserts success")
      assert(response.get._1)
      And("it contains the expected message")
      assert(response.get._2 == responseMessageOK)
    }

    scenario("An invalid signup request is sent to the server") {
      Given("an invalid username and password")
      When("the request is sent to the server")
      val response = blockingSignup(invalidUsername, password)
      Then("the response asserts failure")
      assert(!response.get._1)
      And("it contains the expected error message prepared for display to user")
      assert(response.get._2 == responseMessageErrorDetail)
    }

    scenario("A login request with an invalid parameter is sent to the server") {
      Given("some invalid parameters")
      When("the request is sent to the server")
      val response = blockingLogin(invalidUsername, token)
      Then("the response asserts failure")
      assert(!response.get._1)
      And("it contains the expected error message prepared for display to user")
      assert(response.get._2 == responseMessageErrorDetail)
    }

    scenario("An invalid signup request is sent to the server and it respond with unknown error format") {
      Given("an invalid username and password")
      When("the request is sent to the server")
      val response = blockingSignup(unknownFormatError, password)
      Then("the response asserts failure")
      assert(!response.get._1)
      And("it contains a general error message")
      assert(response.get._2 == generalErrorMessage)
    }

    scenario("A login request with an invalid parameter is sent to the server and it respond with unknown error format") {
      Given("some invalid parameters")
      When("the request is sent to the server")
      val response = blockingLogin(unknownFormatError, token)
      Then("the response asserts failure")
      assert(!response.get._1)
      And("it contains a general error message")
      assert(response.get._2 == generalErrorMessage)
    }

    scenario("A login request is sent but cannot contact the server") {
      When("the request is sent to the server but there is no connection")
      val response = blockingSignup(throwException, password)
      Then("the response is a failure")
      assert(response.isFailure)
      And("it contains the exception message")
      assert(response.failed.get.getMessage == throwException)
    }

    scenario("A registration request is sent but cannot contact the server") {
      When("the request is sent to the server but there is no connection")
      val response = blockingLogin(throwException, token)
      Then("the response is a failure")
      assert(response.isFailure)
      And("it contains the exception message")
      assert(response.failed.get.getMessage == throwException)
    }

    scenario("A login request needs a token in order to be valid") {
      Given("a valid token")
      When("the request is sent to the server")
      blockingLogin(username, token)
      Then("the request contains the token")
      (userApi.addHeader _).verify(authenticationHeader, token)
    }

    scenario("A custom URI can be used in the broker") {
      Given("a uri")
      val customURI = "http://custom.uri.com"
      When("a broker is instantiated")
      val brokerWithCustomURI = BrokerREST(customURI)
      Then("the broker use the specified uri for the requests")
      assert(brokerWithCustomURI.userApi.basePath == customURI)
    }

  }

  val maxWait: Duration = 500 millis

  private def blockingSignup(username: String, password: String): Try[(Boolean, String)] =
    Await.ready(accountServiceBroker.signup(username, password), maxWait).value.get
  private def blockingLogin(username: String, token: String): Try[(Boolean, String)] =
    Await.ready(accountServiceBroker.login(username, token), maxWait).value.get
}
