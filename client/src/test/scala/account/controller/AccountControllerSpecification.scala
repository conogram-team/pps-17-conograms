package account.controller

import java.util.concurrent.{ExecutorService, Executors, TimeUnit}

import account.model.service.{AccountServiceBroker, TokenManager}
import account.model.ConogramUser
import account.view.AccountView
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FeatureSpec, GivenWhenThen, OneInstancePerTest}

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

import account.model.service.support.Network._

class AccountControllerSpecification extends FeatureSpec
  with GivenWhenThen with MockFactory with OneInstancePerTest {

  val username = "Alice"
  val password = "password"
  val wrongPassword = password + "Wrong"
  val token = username + password
  val wrongToken = username + wrongPassword
  val serverToken = token + "123"
  val wrongServerToken = wrongToken + "123"
  val user = ConogramUser[String](username, Some(token), userBoundCompleteAddress)
  val okMessage = "ok"
  val errorMessage = "Invalid credential"
  val exceptionMessage = "Some Exception"
  val throwException = "throwException"
  val wrongField = "a"
  val emptyField = ""

  val tokenManager = stub[TokenManager[String]]
  val accountServiceBroker = stub[AccountServiceBroker]
  val accountView = stub[AccountView]
  val loginObserver = stub[ConogramUser[String] => Unit]

  val maxWait = 1000
  val futureExecutor: ExecutorService = Executors.newFixedThreadPool(1)
  implicit val futureContext: ExecutionContextExecutor = ExecutionContext.fromExecutorService(futureExecutor)
  def waitFuture(): Unit = {futureExecutor.awaitTermination(maxWait, TimeUnit.MILLISECONDS)}

  (tokenManager.buildToken _).when(username, password).returns(token)
  (tokenManager.tokenToString _).when(token).returns(serverToken)
  (tokenManager.buildToken _).when(username, wrongPassword).returns(wrongToken)
  (tokenManager.tokenToString _).when(wrongToken).returns(wrongServerToken)
  (tokenManager.buildToken _).when(throwException, throwException).returns(throwException)
  (tokenManager.tokenToString _).when(throwException).returns(throwException)

  (accountServiceBroker.login _).when(username, serverToken).returns(Future((true, okMessage)))
  (accountServiceBroker.login _).when(username, wrongServerToken).returns(Future((false, errorMessage)))
  (accountServiceBroker.login _).when(throwException, throwException).returns(Future.failed(new Exception(exceptionMessage)))
  (accountServiceBroker.signup _).when(username, password).returns(Future((true, okMessage)))
  (accountServiceBroker.signup _).when(wrongField, wrongField).returns(Future((false, errorMessage)))
  (accountServiceBroker.signup _).when(throwException, throwException).returns(Future.failed(new Exception(exceptionMessage)))

  (accountView.close _).when()
  (accountView.informError _).when(_: String)

  (loginObserver.apply _).when(user)

  val accountController: AccountController[String] = AccountController(
    tokenManager = tokenManager,
    accountServiceBroker = accountServiceBroker,
    accountView = accountView
  )

  feature("The user can signup and login") {

    info("As a User")
    info("I want to log in to the system")
    info("In order to play and interact with other players")

    scenario("A user require to login so the credentials must be verified") {
      Given("a username and password")
      When("the user apply for login")
      accountController.login(username, password)
      waitFuture
      Then("the credentials are used to compute the token")
      (tokenManager.buildToken _).verify(username, password)
      And("a request is sent to the server in order to validate the credentials")
      (accountServiceBroker.login _).verify(username, serverToken)
    }

    scenario("The system backbone must receive the users data when he log in") {
      Given("an observer that subscribe to login status update")
      accountController.addLoginObserver(loginObserver)
      When("the user provide correct credentials")
      accountController.login(username, password)
      waitFuture
      Then("the observer receive the user data")
      (loginObserver.apply _).verify(user)
    }

    scenario("After a successful login the view must be closed") {
      When("the login is successful")
      accountController.login(username, password)
      waitFuture
      Then("the view is closed")
      (accountView.close _).verify()
    }

    scenario("When a user provide wrong credential, he must be informed") {
      When("the login fails")
      accountController.login(username, wrongPassword)
      waitFuture
      Then("the view is required to display a message")
      (accountView.informError _).verify(errorMessage)
    }

    scenario("When a connection can't be established, the user must be informed") {
      When("the login request fails")
      accountController.login(throwException, throwException)
      waitFuture
      Then("the view is required to display a message")
      (accountView.informError _).verify(*)
    }

    scenario("A user require to signup") {
      Given("a valid username and password")
      When("the user apply for signup")
      accountController.signup(username, password)
      waitFuture
      Then("a request is sent to the server in order to register the new user")
      (accountServiceBroker.signup _).verify(username, password)
    }

    scenario("After a valid signup, the user is automatically logged in") {
      Given("a valid username and password")
      When("the user complete a signup")
      accountController.signup(username, password)
      waitFuture
      Then("the procedure of login is started")
      (accountServiceBroker.login _).verify(username, serverToken)
    }

    scenario("When a parameter doesn't meet the server criteria the user must be informed") {
      Given("an invalid username or password")
      When("the user try to a signup")
      accountController.signup(wrongField, wrongField)
      waitFuture
      Then("the view is required to display a message")
      (accountView.informError _).verify(errorMessage)
    }

    scenario("When a connection can't be established during registration, the user must be informed") {
      When("the signup request fails")
      accountController.signup(throwException, throwException)
      waitFuture
      Then("the view is required to display a message")
      (accountView.informError _).verify(*)
    }

    scenario("The user doesn't provide one o more fields for login") {
      Given("an empty username or password field")
      When("the user apply for login")
      accountController.login(emptyField, emptyField)
      waitFuture
      Then("the procedure is not called and the user is informed")
      (accountView.informError _).verify(*)
    }

    scenario("The user doesn't provide one o more fields for registration") {
      Given("an empty username or password field")
      When("the user apply for registration")
      accountController.signup(emptyField, emptyField)
      waitFuture
      Then("the procedure is not called and the user is informed")
      (accountView.informError _).verify(*)
    }

  }
}
