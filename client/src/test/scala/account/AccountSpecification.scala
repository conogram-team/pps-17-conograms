package account

import account.model.ConogramUser
import org.scalatest.{FeatureSpec, GivenWhenThen}

class AccountSpecification extends FeatureSpec with GivenWhenThen {

  val username = "Alice"
  val user = ConogramUser[String](username)

  feature("Account singleton to get the logged user from entire application") {

    info("As a Programmer")
    info("I want to obtain the user")
    info("So that i can use it for related procedure")

    scenario("The Account object track the user") {
      When("the user is set")
      Account.currentUser = Some(user)
      Then("the user can be obtained")
      assert(Account.currentUser.get == user)
    }
  }
}