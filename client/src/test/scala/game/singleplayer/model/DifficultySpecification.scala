package game.singleplayer.model

import game.singleplayer.model.Difficulty._
import org.scalatest.{FeatureSpec, GivenWhenThen}

class DifficultySpecification extends FeatureSpec with GivenWhenThen {

  val rows = 10
  val columns = 10
  val totalCells = rows * columns
  val lowDifficultyPercent = 0.7
  val mediumDifficultyPercent = 0.5
  val highDifficultyPercent = 0.3

  feature("The density of cells is proportional to selected difficulty") {

    info("As a User")
    info("I want to play a grid with a cells number proportional to selected difficulty")
    info("So that i can play with the right difficulty to my level")

    scenario("The selected difficulty is low") {
      Given("a low difficulty")
      val selectedDifficulty = Low
      When("the number of cell is counted for selected dimension")
      val fullCells = selectedDifficulty.fullCellsOf(rows, columns)
      Then("the result is the 70% of total number of cells")
      assert(fullCells == totalCells * lowDifficultyPercent )
    }

    scenario("The selected difficulty is medium") {
      Given("a medium difficulty")
      val selectedDifficulty = Medium
      When("the number of cell is counted for selected dimension")
      val fullCells = selectedDifficulty.fullCellsOf(rows, columns)
      Then("the result is the 50% of total number of cells")
      assert(fullCells == totalCells * mediumDifficultyPercent )
    }

    scenario("The selected difficulty is high") {
      Given("a high difficulty")
      val selectedDifficulty = High
      When("the number of cell is counted for selected dimension")
      val fullCells = selectedDifficulty.fullCellsOf(rows, columns)
      Then("the result is the 30% of total number of cells")
      assert(fullCells == totalCells * highDifficultyPercent )
    }
  }
}
