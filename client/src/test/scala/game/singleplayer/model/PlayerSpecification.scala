package game.singleplayer.model

import account.model.ConogramUser
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class PlayerSpecification extends FeatureSpec with GivenWhenThen with BeforeAndAfter {
  var player:Player = _

  before{
    player=Player(ConogramUser("username"))
  }

  feature("The player") {

    info("As a player")
    info("I would like to have a maximum number of lives")

    scenario("the player must return the number of available lives "){
    Given("The number of available lives")
     val lives= player.getAvailableLives
      When("the player loses a life")
      val updateLives= player.decrementAvailableLives()
      Then("return the lives number update ")
      assert(player.getAvailableLives == lives - 1 )
    }
  }

  feature("player") {

    info("As a player")
    info("I would like to save the right number of cells")

    scenario("The player must return the number of right selection"){
      Given("A cell")
      val lives= player.addSelectedCellIn(10,10)
      When("the player has selected a right cell")
      val updateLives= player.incrementRightSelectionNumber
      Then("return the lives number update ")
      assert(player.rightSelectionNumber == 1 )
    }
  }


}
