package game.singleplayer.model.grid.gridfactory

import game.singleplayer.model.grid.{Axis, Grid}
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class RandomGridFactorySpecification extends FeatureSpec with GivenWhenThen with BeforeAndAfter {

  val Row = 10
  val Column = 10
  val FullCell = 20
  val ExceedFullCell = Row * Column + 1

  var grid: Grid[Boolean] = _
  before {
    grid = BooleanRandomGridFactory(Row, Column, FullCell)
  }

  feature("Generation of random grid") {

    info("As a user")
    info("I want to generate a random grid")
    info("So that I can play every time with a new board")

    scenario("Grid generation is invoked with invalid row size") {
      When("grid generation is invoked with row < 1")
      Then("illegalArgumentException should be thrown")
      intercept[IllegalArgumentException] {
        BooleanRandomGridFactory(0, 1, 1)
      }
    }

    scenario("Grid generation is invoked with invalid column size") {
      When("grid generation is invoked with column < 1")
      Then("illegalArgumentException should be thrown")
      intercept[IllegalArgumentException] {
        BooleanRandomGridFactory(1, 0, 1)
      }
    }

    scenario("Grid generation is invoked with invalid full cell number") {
      When("grid generation is invoked with full cells < 0")
      Then("illegalArgumentException should be thrown")
      intercept[IllegalArgumentException] {
        BooleanRandomGridFactory(1, 1, -1)
      }
    }

    scenario("Grid generation is invoked with too much full cells") {
      When("grid generation is invoked with full cells > actual total of cells")
      Then("illegalArgumentException should be thrown")
      intercept[IllegalArgumentException] {
        BooleanRandomGridFactory(Row, Column, ExceedFullCell)
      }
    }

    scenario("A random grid is generated with proper parameters") {
      Given("a random grid")
      When("grid generation is invoked with proper parameters")
      Then("the grid size is greater than zero")
      assert(grid.getSize._1 > 0 && grid.getSize._2 > 0)
    }

    scenario("A random grid is generated and it contains the specified number of full cells") {
      Given("a random grid")
      Then("the grid contains the exactly number of full cells")
      val fullCellCounter = for(
        row <- 0 until Row;
        column <- 0 until Column
        if grid.getCellStatusIn(row, column).right.get) yield true

      assert(fullCellCounter.length == FullCell)
    }

    scenario("A random grid is generated and it contains the hints") {
      Given("a random grid")
      Then("the grid contains some hints for all the row and the column")
      val HintCounter = for(
        row <- 0 to Row;
        column <- 0 to Column
        if grid.getHints(Axis.Horizontal, row).nonEmpty && grid.getHints(Axis.Vertical, column).nonEmpty) yield true
      
      assert(HintCounter.length == Row * Column)
    }

  }
}