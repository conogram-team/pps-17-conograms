package game.singleplayer.model.grid

import Grid.{AxisHints, Matrix}
import org.scalatest.{FeatureSpec, GivenWhenThen}

private case class Color(red: Int, green: Int, blue: Int)

class GridSpecification extends FeatureSpec with GivenWhenThen {
  implicit val defaultStatus: Boolean = false
  val numberOfRows = 5
  val numberOfColumns = 5
  val numberOfDoubleColumns: Int = numberOfColumns * 2

  info("As a client, I would like to create and manage an immutable grid.")
  info("The grid works well in both rectangular and square format.")
  info("The grid can be filled with any data type.")

  feature("The client create an immutable grid and update it") {

    scenario("The client create an immutable grid and add some vertical hints") {
      Given("a grid and vertical hints")
      val matrix = getFilledMatrixAndHints._1
      val verticalHints = Map(
        0 -> Seq((3, true)),
        4 -> Seq((1, true), (2, true))
      )
      val grid: Grid[Boolean] = Grid[Boolean](matrix, verticalHints)

      When("the client add a new hint")
      val updatedGrid = grid.setHints(Axis.Vertical, 2, Seq((4, true)))

      Then("the grid and updatedGrid are not equal")
      assert(updatedGrid !== grid)
    }

  }

  feature("The client wants to create a typed grid with all the cells set to the same default value") {

    scenario("The client creates a boolean grid with 'false' value in all its cells") {
      Given("the number of rows and columns")
      When("the client creates the grid")
      val grid: Grid[Boolean] = Grid[Boolean](numberOfRows, numberOfColumns)

      Then("the grid have all cells set to false")
      assert(allCellsHaveTheValue(grid))
    }

    scenario("The client create a colored grid with white color in all its cells") {
      Given("the number of rows and columns")
      When("the client create the grid")
      val red = 255
      val green = 255
      val blue = 255
      implicit val defaultStatus: Color = Color(red, green, blue)
      val grid: Grid[Color] = Grid[Color](numberOfRows, numberOfColumns)

      Then("the grid have all cells set to white")
      assert(allCellsHaveTheValue(grid))
    }

  }

  feature("The client wants to create a filled grid given a matrix") {

    scenario("The client creates a boolean square grid with 'true' value in all its cells along the diagonal") {
      Given("a matrix with diagonal set to true")
      val matrix = getMatrixWithTrueDiagonal(numberOfRows, numberOfColumns)

      When("the client create the grid")
      val grid: Grid[Boolean] = Grid[Boolean](matrix)

      Then("the diagonal have the same size of the rows")
      assert(numberOfCellsAlongDiagonal(grid) === grid.getSize._1)

      And("the diagonal have the same size of the columns")
      assert(numberOfCellsAlongDiagonal(grid) === grid.getSize._2)
    }

    scenario("The client creates a boolean rectangular grid") {
      Given("a rectangular matrix")
      val matrix = getMatrixWithTrueDiagonal(numberOfRows, numberOfDoubleColumns)

      When("the client create the grid")
      val grid: Grid[Boolean] = Grid[Boolean](matrix)

      Then("the diagonal have the same size of the rows")
      val cellsAlongDiagonal = numberOfCellsAlongDiagonal(grid)
      assert(cellsAlongDiagonal === grid.getSize._1)

      And("the diagonal size is less than the number of columns")
      assert(cellsAlongDiagonal < grid.getSize._2)
    }

  }

  feature("The client wants to manage the status of a cell") {

    scenario("The client requests the status of a non-existing cell") {
      Given("a grid")
      val matrix = getMatrixWithTrueDiagonal(numberOfRows, numberOfColumns)
      val grid = Grid[Boolean](matrix)

      When("the client requests the status of cell at the position (1,5)")
      val row = 1
      val column = 5
      val status = grid.getCellStatusIn(row, column)

      Then("the cell doesn't existing")
      assert(status.isLeft)
    }

    scenario("The client update the status of a non-existing cell") {
      Given("a grid")
      val matrix = getMatrixWithTrueDiagonal(numberOfRows, numberOfColumns)
      val grid = Grid[Boolean](matrix)

      When("the client set the status of cell at the position (1,5)")
      val status = grid.setCellStatus((1,5), true)

      Then("the cell doesn't existing")
      assert(status == Left(EitherMessage.PositionNotFound))
    }

    scenario("The client updates the status of a cell such that the new state is the same as the old one") {
      Given("a grid")
      val matrix = getMatrixWithTrueDiagonal(numberOfRows, numberOfColumns)
      val grid = Grid[Boolean](matrix)

      When("the client set the status of cell at the position (1,1)")
      Then("the cell doesn't existing")
      assert(grid === grid.setCellStatus((1,1), true).right.get)
    }

  }

  feature("The client wants to create a grid with the hints") {

    scenario("The client create a grid with only vertical hints") {
      Given("a matrix and vertical hints")
      val matrix = getFilledMatrixAndHints._1
      val verticalHints = getFilledMatrixAndHints._2

      When("the client create the grid")
      val grid: Grid[Boolean] = Grid[Boolean](matrix = matrix, verticalAxisHints = verticalHints)

      Then("the vertical hints of the grid are the same to the vertical hints immutable variable")
      assert(grid.getAllHints(Axis.Vertical) === verticalHints)
    }

    scenario("The client create a grid with only horizontal hints") {
      Given("a matrix and horizontal hints")
      val matrix = getFilledMatrixAndHints._1
      val horizontalHints = getFilledMatrixAndHints._3

      When("the client create the grid")
      val grid: Grid[Boolean] = Grid[Boolean](matrix = matrix, horizontalAxisHints = horizontalHints)

      Then("the horizontal hints of the grid are the same to the horizontal hints immutable variable")
      assert(grid.getAllHints(Axis.Horizontal) === horizontalHints)
    }

    scenario("The client create a grid with both vertical and horizontal hints") {
      Given("a matrix and the vertical hints and horizontal hints")
      val matrix = getFilledMatrixAndHints._1
      val verticalHints = getFilledMatrixAndHints._2
      val horizontalHints = getFilledMatrixAndHints._3

      When("the client create the grid")
      val grid: Grid[Boolean] = Grid[Boolean](matrix = matrix, verticalAxisHints = verticalHints, horizontalAxisHints = horizontalHints)

      Then("the horizontal hints of the grid are the same to the horizontal hints immutable variable")
      val allHints = (grid.getAllHints(Axis.Vertical), grid.getAllHints(Axis.Horizontal))
      val knownHints = (getFilledMatrixAndHints._2, getFilledMatrixAndHints._3)
      assert(allHints == knownHints)
    }

  }

  feature("The client wants to manage the status of a cell") {

    scenario("The client set the status of a cell") {
      Given("a grid")
      val grid = Grid[Boolean](numberOfRows, numberOfColumns)

      When("the client set the status of the cell (1,4) to 'true' value")
      val row = 1
      val column = 4
      val gridUpdated = grid.setCellStatus((row, column), true).right.get

      Then("the cell (1,4) of the grid has the value set to 'true'")
      assert(gridUpdated.getCellStatusIn(row, column) === Right(true))
    }

  }

  feature("The client wants to retrieve the size of grid") {

    info("The grid size is expressed as (row x column)")

    scenario("The client retrieve the size of grid") {
      Given("a grid")
      val grid = Grid[Boolean](numberOfRows, numberOfColumns)

      When("the client retrieve the size of the grid")
      val size = grid.getSize

      Then("the size of grid is equal to the size of the matrix")
      assert(size === (numberOfRows, numberOfColumns))
    }

  }

  feature("The client wants to retrieve the number of the full cells") {

    info("The full cells are those cells with a status other than the default one")

    scenario("the client retrieve the number of the full cells") {
      Given("a grid with vertical hints")
      val matrix = getFilledMatrixAndHints._1
      val verticalHints = getFilledMatrixAndHints._2

      val grid = Grid[Boolean](matrix, verticalHints)

      Then("the number of complete cells obtained from the grid is the same as that of the matrix")
      assert(grid.getNumberOfFullCells === 20)
    }

  }

  private def getMatrixWithTrueDiagonal(numberOfRows: Int, numberOfColumns: Int) =
    Seq.tabulate[Boolean](numberOfRows, numberOfColumns)((row, column) => row == column)


  private def numberOfCellsAlongDiagonal[T](grid: Grid[T]): Int = {
    val rows = 0 until grid.getSize._1
    val columns = 0 until grid.getSize._2

    rows.map(row => columns.count(column => column == row)).sum
  }


  private def allCellsHaveTheValue[T](grid: Grid[T])(implicit value: T): Boolean = {
    val rows = 0 until grid.getSize._1
    val columns = 0 until grid.getSize._2

    rows.map(row => columns
      .forall(column => grid.getCellStatusIn(row, column) == Right(value)))
      .forall(rightRow => rightRow)
  }


  private def getFilledMatrixAndHints: (Matrix[Boolean], AxisHints[Boolean], AxisHints[Boolean]) = {
    val matrix = Matrix(
      Row(true, false, true, false, true, true, true, false, true, false),
      Row(true, true, false, false, true, true, false, true, false, false),
      Row(true, false, false, false, true, false, false, true, false, false),
      Row(false, true, false, false, true, true, false, false, false, false),
      Row(false, true, false, false, false, true, false, true, false, false)
    )

    implicit val defaultStatus: Boolean = true
    val verticalHints = AxisHints[Boolean](0 -> Seq(3), 1 -> Seq(1, 2),
      2 -> Seq(1), 4 -> Seq(4), 5 -> Seq(2, 2), 6 -> Seq(1), 7 -> Seq(2, 1), 8 -> Seq(1))

    val horizontalHints = AxisHints[Boolean](0 -> Seq(1, 1, 3, 1),
      1 -> Seq(2, 2, 1), 2 -> Seq(1, 1, 1), 3 -> Seq(1, 2), 4 -> Seq(1, 1, 1))

    (matrix, verticalHints, horizontalHints)
  }

}
