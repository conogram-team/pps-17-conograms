package game.multiplayer.controller

import account.Account
import account.model.ConogramUser
import account.model.service.support.Network
import game.singleplayer.model.Player
import game.singleplayer.model.grid.gridfactory.BooleanRandomGridFactory
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class MatchControllerSpecification extends FeatureSpec with GivenWhenThen with BeforeAndAfter {

  private val size = 5
  private val fullCellsNumber = 10
  private val grid = BooleanRandomGridFactory(size, size, fullCellsNumber)
  private val challenger = Player(ConogramUser("carl"))

  Account.currentUser_=(Some(ConogramUser("alice", None, Some(Network.localAddress))))
  Network.bindRemoteSystem(Account.currentUser.get.location.get, 1234)

  info("As a player, I want to win a match when I choose rightly all the full cells of the grid")

  feature("Win of a match") {
    scenario("User chooses rightly all the cells of the grid") {
      Given("a match and the number of cells that have been choosen rightly")
      val winnerPlayer: Player = Player(Account.currentUser.get)
      val winnerMatchController = MultiplayerMatchController(grid)
      winnerMatchController.localPlayer = winnerPlayer
      winnerMatchController.addPlayerInMatch(challenger)

      for (row <- 0 until size; column <- 0 until size) yield {
        grid.getCellStatusIn(row, column) match {
          case Right(status) => winnerMatchController.selectCellIn((row, column), true, winnerPlayer)
          case _ =>
        }
      }

      When("that number is equals to the number of the full cells in the grid")

      Then("the user wins the match")
      assert(!winnerMatchController.getWinners.contains(winnerPlayer))
    }

    scenario("User chooses rightly some of the cells of the grid (but not at all)") {
      Given("a match and the number of cells that have been choosen rightly")
      val player: Player = Player(Account.currentUser.get)
      val matchController = MultiplayerMatchController(grid)
      matchController.localPlayer = player
      matchController.addPlayerInMatch(challenger)

      for (row <- 0 until size - 1; column <- 0 until size - 1) yield {
        grid.getCellStatusIn(row, column) match {
          case Right(status) => matchController.selectCellIn((row, column), true)
          case _ =>
        }
      }


      When("that number is equals to the number of the full cells in the grid")

      Then("the user wins the match")
      assert(!matchController.getWinners.contains(player))
    }
  }
}
