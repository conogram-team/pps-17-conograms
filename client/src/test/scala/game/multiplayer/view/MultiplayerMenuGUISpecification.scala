package game.multiplayer.view

//noinspection ScalaStyle
import java.awt.Color

import game.singleplayer.model.Difficulty
import game.singleplayer.view.MenuView
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen, OneInstancePerTest}

class MultiplayerMenuGUISpecification extends FeatureSpec
  with GivenWhenThen with BeforeAndAfter with MockFactory with OneInstancePerTest {

  val opponent = "Bob"
  val rowsText = "10"
  val columnsText = "10"

  var multiplayerGUI: MultiplayerMenuGUI = _
  val opponentChallengeObserver = stub[String => Unit]
  val randomChallengeObserver = stub[() => Unit]

  (opponentChallengeObserver.apply _).when(_: String)
  (randomChallengeObserver.apply _).when()

  before {
    multiplayerGUI = new MultiplayerMenuGUI()
    multiplayerGUI.addUserChallengeObserver(opponentChallengeObserver)
    multiplayerGUI.addORandomChallengeObserver(randomChallengeObserver)
  }

  feature("Multiplayer GUI") {

    info("As a User")
    info("I want a view for challenge other player")
    info("In order to choose grid settings and the opponent that i want to challenge")

    scenario("For the sake of user experience the single and multi player view must be unified") {
      When("the multiplayer menu is opened")
      Then("it contains also the single player selection form")
      assert(multiplayerGUI.isInstanceOf[MenuView])
    }

    scenario("A user click the challenge opponent button") {
      Given("a view with something in opponent field (even empty text) and correct dimension field")
      multiplayerGUI.opponentTextField.text = opponent
      multiplayerGUI.rowsTextField.text = rowsText
      multiplayerGUI.columnsTextField.text = columnsText
      When("the user click the challenge opponent button")
      multiplayerGUI.opponentChallengeButton.doClick()
      Then("the observers are notified with the content of opponent field")
      (opponentChallengeObserver.apply _).verify(opponent)
    }

    scenario("A user click the challenge random opponent button") {
      Given("the dimension fields correctly filled")
      multiplayerGUI.rowsTextField.text = rowsText
      multiplayerGUI.columnsTextField.text = columnsText
      When("the user click the random challenge button")
      multiplayerGUI.randomChallengeButton.doClick()
      Then("the observers are notified")
      (randomChallengeObserver.apply _).verify()
    }

    scenario("A user click the challenge opponent button but he doesn't correctly fill the dimension fields") {
      Given("a view with dimension fields not filled")
      multiplayerGUI.opponentTextField.text = opponent
      When("the user click the challenge opponent button")
      multiplayerGUI.opponentChallengeButton.doClick()
      Then("the observers aren't notified")
      (opponentChallengeObserver.apply _).verify(*).never
      And("the dimension label become red")
      assert(multiplayerGUI.chooseDimensionLabel.foreground == Color.RED)
    }

    scenario("A user click the challenge random opponent button but he doesn't correctly fill the dimension fields") {
      Given("a view with dimension fields not filled")
      When("the user click the random challenge button")
      multiplayerGUI.randomChallengeButton.doClick()
      Then("the observers aren't notified")
      (randomChallengeObserver.apply _).verify().never
      And("the dimension label become red")
      assert(multiplayerGUI.chooseDimensionLabel.foreground == Color.RED)
    }

    scenario("The dimension label color is restored when the user click the challenge opponent button") {
      Given("the dimension fields correctly filled and the dimension label colored of red")
      multiplayerGUI.rowsTextField.text = rowsText
      multiplayerGUI.columnsTextField.text = columnsText
      multiplayerGUI.chooseDimensionLabel.foreground = Color.RED
      When("the user click the challenge opponent button")
      multiplayerGUI.opponentChallengeButton.doClick()
      Then("the dimension label become black")
      assert(multiplayerGUI.chooseDimensionLabel.foreground == Color.BLACK)
    }

    scenario("The dimension label color is restored when the user click the challenge random opponent button") {
      Given("the dimension fields correctly filled and the dimension label colored of red")
      multiplayerGUI.rowsTextField.text = rowsText
      multiplayerGUI.columnsTextField.text = columnsText
      multiplayerGUI.chooseDimensionLabel.foreground = Color.RED
      When("the user click the challenge random opponent button")
      multiplayerGUI.randomChallengeButton.doClick()
      Then("the dimension label become black")
      assert(multiplayerGUI.chooseDimensionLabel.foreground == Color.BLACK)
    }

    scenario("The user set rows number field") {
      Given("the rows field correctly filled")
      multiplayerGUI.rowsTextField.text = rowsText
      Then("the gui provide the number of rows")
      assert(multiplayerGUI.getGridRows == rowsText.toInt)
    }

    scenario("The user set columns number field") {
      Given("the columns field correctly filled")
      multiplayerGUI.columnsTextField.text = columnsText
      Then("the gui provide the number of rows")
      assert(multiplayerGUI.getGridColumns == columnsText.toInt)
    }

    scenario("The user set a difficulty") {
      Given("the selected difficulty")
      val selectedDifficulty = multiplayerGUI.easyValue
      multiplayerGUI.boxValueDifficulty.select(selectedDifficulty)
      Then("the gui provide the difficulty")
      assert(multiplayerGUI.getGridDifficulty == Difficulty.withName(selectedDifficulty.text))
    }
  }
}
