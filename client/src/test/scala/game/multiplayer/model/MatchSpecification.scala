package game.multiplayer.model

import account.Account
import account.model.ConogramUser
import game.singleplayer.model.Player
import game.singleplayer.model.grid.gridfactory.BooleanRandomGridFactory
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class MatchSpecification extends FeatureSpec with GivenWhenThen with BeforeAndAfter {

  private val size = 5
  private val fullCellsNumber = 10
  private val grid = BooleanRandomGridFactory(size, size, fullCellsNumber)
  Account.currentUser_=(Some(ConogramUser("alice")))

  info("As a player, I want to select a cell that I believe is full")

  feature("Cell selection") {
    scenario("User presses into a cell which has not been clicked yet") {
      val defaultStatus = true
      var selectionStatus: Boolean = true

      Given("a match")
      val matchModel: MultiplayerMatch[Boolean] = MultiplayerMatchModel(grid)
      matchModel.localPlayer_=(Player(Account.currentUser.get))

      Given("the rows and the columns of the cell in the grid of the match")

      When("the cell is clicked")

      Then("check if the supposed status of the cell agrees with the effective status of the cell in the grid")
      for (row <- 0 until size; column <- 0 until size) yield {
        grid.getCellStatusIn(row, column) match {
          case Right(status) => if (status != matchModel.checkCellStatusIn((row, column), defaultStatus))
            selectionStatus = false
          case _ =>
        }
      }

      assert(selectionStatus)
    }
  }

  info("As a player, I want to win a match when I choose rightly all the full cells of the grid")

  feature("Win of a match") {
    scenario("User chooses rightly all the cells of the grid") {

      Given("a match")
      val matchModel: MultiplayerMatch[Boolean] = MultiplayerMatchModel(grid)
      matchModel.localPlayer_=(Player(Account.currentUser.get))

      Given("the number of cells that have been choosen rightly")

      When("that number is equals to the number of the full cells in the grid")
      for (row <- 0 until size; column <- 0 until size) yield {
        grid.getCellStatusIn(row, column) match {
          case Right(true) => matchModel.localPlayer.get.incrementRightSelectionNumber()
          case _ =>
        }
      }

      Then("the user wins the match")

      assert(matchModel.hasThePlayerWon())
    }

    scenario("User chooses rightly some of the cells of the grid (but not at all)") {

      Given("a match")
      val matchModel: MultiplayerMatch[Boolean] = MultiplayerMatchModel(grid)
      matchModel.localPlayer_=(Player(Account.currentUser.get))

      Given("the number of cells that have been choosen rightly")

      When("that player had selected some of the cells of the grid")
      for (row <- 0 until size; column <- 0 until size - 1) yield {
        grid.getCellStatusIn(row, column) match {
          case Right(true) => matchModel.localPlayer.get.addSelectedCellIn(row, column)
          case _ =>
        }
      }

      Then("the user wins the match")
      assert(!matchModel.hasThePlayerWon())
    }
  }
}
