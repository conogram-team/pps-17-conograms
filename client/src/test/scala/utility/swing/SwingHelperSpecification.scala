package utility.swing

//noinspection ScalaStyle
import java.awt.event.ActionEvent

import javax.swing.{JButton, JFrame, SwingUtilities}
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}
import SwingHelper._

import scala.swing.Dimension

class SwingHelperSpecification extends FeatureSpec with GivenWhenThen with BeforeAndAfter {

  val button: JButton = new JButton("button")
  val secondButton: JButton = new JButton("secondButton")
  var executed = false
  var onEDT = false
  var castEvent: Option[ActionEvent] = None

  before{
    executed = false
    onEDT = false
  }

  feature("Swing helper for integration in Scala") {

    info("As a Programmer")
    info("I want to use Swing in a smooth way")
    info("In order to build a GUI")

    scenario("A function is registered as a component handler") {
      Given("a GUI component")
      When("a function is added")
      button.addActionListener( {executed = true} )
      Then("the function is executed when a event occurs")
      assert(!executed)
      button.doClick()
      assert(executed)
    }

    scenario("A function that consume an event is registered as a component handler") {
      Given("a GUI component")
      When("a function that consume an event is added")
      button.addActionListener({ event:ActionEvent => {castEvent = Some(event)} })
      Then("the function is executed when a event occurs and it can use the event")
      button.doClick()
      assert(castEvent.nonEmpty)
    }

    scenario("A couple of int is used as a dimension") {
      Given("a couple of int")
      val width = 3
      val height = 5
      When("they are used as a dimension")
      val dimension: Dimension = (width, height)
      Then("the dimension contains the correct width")
      assert(dimension.width == width)
      And("the dimension contains the correct height")
      assert(dimension.height == height)
    }

    scenario("All the components are added to a frame") {
      Given("a frame and a list of components")
      val frame = new JFrame()
      When("the components are added to the frame")
      frame.addAllComponent(button, secondButton)
      Then("the frame contains all the components")
      assert(frame.getContentPane.getComponents.contains(button) &&
        frame.getContentPane.getComponents.contains(secondButton))
    }

    scenario("A function must be performed on EDT") {
      When("a function is executed with safeInvokeAndWait")
      safeInvokeAndWait( {onEDT = SwingUtilities.isEventDispatchThread} )
      Then("the function is executed in the EDT")
      assert(onEDT)
    }

    scenario("A function must be performed on EDT but the thread is already the EDT") {
      When("a function is executed with safeInvokeAndWait within the EDT")
      SwingUtilities.invokeAndWait(safeInvokeAndWait( {executed = true} ))
      Then("the function is executed")
      assert(executed)
    }

  }
}
