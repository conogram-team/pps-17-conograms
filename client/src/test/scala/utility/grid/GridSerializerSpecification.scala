package utility.grid

import game.singleplayer.model.grid.Grid
import org.scalatest.{FeatureSpec, GivenWhenThen}

class GridSerializerSpecification extends FeatureSpec with GivenWhenThen {

  info("As a programmer")
  info("I would convert the grid into string")
  info("for debug or log purposes")

  private val numberOfRows = 2
  private val numberOfColumns = 2
  private val matrix = Seq.tabulate(numberOfRows, numberOfColumns)((row, column) => row == column)

  feature("The client wants convert a grid into a string") {

    scenario("The client wants convert a grid into a simple string") {
      Given("a grid")
      val grid = Grid[Boolean](matrix)

      When("the client convert it into string")
      val gridToString = GridSerializer.toString(grid)

      Then("the string is equal to [Number of rows: value, number of columns: value, number of full cells: value]")
      assert(gridToString === s"[Number of rows: $numberOfRows, number of columns: $numberOfColumns, number of full cells: 0]")
    }

    scenario("The client wants convert a grid into a detailed string") {
      Given("a grid")
      val grid = Grid[Boolean](matrix)

      When("the client convert it into string")
      val gridToString = GridSerializer.toStringDetailed(grid)

      Then("the string is equal to (row: value, column: value, status: value) for each cells")
      assert(gridToString === stringDetailedFormat)
    }

    scenario("The client wants to convert a grid into a matrix format string") {
      Given("a grid")
      val grid = Grid[Boolean](matrix)

      When("the client convert it into string")
      val gridToString = GridSerializer.toStringAsMatrix(grid)

      Then("the string is in a matrix format")
      assert(gridToString === stringMatrixFormat)
    }
  }

  private def stringDetailedFormat: String =
    s"(row: 0, column: 0) => status: true\n" concat
    s"(row: 0, column: 1) => status: false\n" concat
    s"(row: 1, column: 0) => status: false\n" concat
    s"(row: 1, column: 1) => status: true\n"

  private def stringMatrixFormat: String =
    "\ntrue\tfalse\t" concat
    "\nfalse\ttrue\t"
}
