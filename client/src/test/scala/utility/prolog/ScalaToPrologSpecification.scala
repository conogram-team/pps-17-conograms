package utility.prolog

import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class ScalaToPrologSpecification extends FeatureSpec with GivenWhenThen with BeforeAndAfter {

  import alice.tuprolog._
  import ScalaToProlog._

  val term = "X"
  val expectedResult = "a"
  val goal = s"search($term,[$expectedResult,b,c])"
  val failGoal = "search(d,[a,b,c])"
  val basicGoal = "append([a], [b], [a,b])"
  val theory = """
      search(E,[E|_]).
      search(E,[_|L]):-search(E,L).
    """

  var engine: Term => Stream[SolveInfo] = _

  before {
    engine = makePrologEngine(theory)
  }

  feature("tuProlog integration") {

    info("As a programmer")
    info("I want to be able to load a Prolog theory")
    info("So that I can submit goals and get solutions")

    scenario("A simple Prolog theory is loaded and a successful goal is submitted") {
      Given("a prolog engine")
      When("a successful goal is submitted")
      Then("solve succeed")
      assert(solveWithSuccess(engine, goal))
      And("the term contains the expected value")
      assert(solveOneAndGetTerm(engine, goal, term).get == stringToTerm(expectedResult))
    }

    scenario("A successful goal is submitted and a term is extracted by index") {
      Given("a prolog engine")
      When("a successful goal is submitted")
      Then("the term extracted by index contains the expected value")
      assert(solveOneAndGetTerm(engine, goal, 0).get == stringToTerm(expectedResult))
    }

    scenario("A simple Prolog theory is loaded and a fail goal is submitted") {
      Given("a prolog engine")
      When("a fail goal is submitted")
      Then("solve doesn't succeed")
      assert(!solveWithSuccess(engine, failGoal))
      And("the term is empty")
      assert(solveOneAndGetTerm(engine, failGoal, term).isEmpty)
    }

    scenario("A Prolog engine without a theory provides built-in predicates") {
      Given("a Prolog engine without a theory")
      engine = makePrologEngine
      When("a successful goal that use a built-in predicate is submitted")
      Then("solve succeed")
      assert(solveWithSuccess(engine, basicGoal))
    }

    scenario("A sequence can be used as a Prolog list and vice versa") {
      Given("a sequence")
      val sequence = Seq("a", "b", "c")
      When("it is converted to a term")
      val termFromSequence: Term = seqToTerm(sequence)
      Then("The term can be reconverted to the sequence")
      assert(prologListToSeq(termFromSequence) == sequence)
    }
  }
}