name := "client"

version := "0.1"
scalaVersion in ThisBuild := "2.11.12"

lazy val akkaVersion = "2.3.16"

libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic" % "3.0.4" % Test,
  "org.scalatest" %% "scalatest" % "3.0.4" % Test,
  "org.scalamock" %% "scalamock" % "4.1.0" % Test,
  "junit" % "junit" % "4.12" % "test",

  "it.unibo.alice.tuprolog" % "tuprolog" % "3.3.0",
  "org.scala-lang.modules" %% "scala-swing" % "2.0.0-M2",
  "com.pauldijou" %% "jwt-core" % "0.19.0",

  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-remote" % akkaVersion,
  "com.github.romix.akka" %% "akka-kryo-serialization" % "0.3.3",

  "io.swagger" % "swagger-core" % "1.5.8",
  "com.wordnik.swagger" %% "swagger-async-httpclient" % "0.3.5",

  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.2",
  "com.fasterxml.jackson.datatype" % "jackson-datatype-joda" % "2.9.2",
  "com.sun.jersey" % "jersey-core" % "1.19.4",
  "com.sun.jersey" % "jersey-client" % "1.19.4",
  "com.sun.jersey.contribs" % "jersey-multipart" % "1.19.4",
  "org.jfarcand" % "jersey-ahc-client" % "1.0.5",
  "joda-time" % "joda-time" % "2.9.9",
  "org.joda" % "joda-convert" % "1.9.2",

  "ch.qos.logback" % "logback-classic" % "1.2.3"
)

resolvers ++= Seq(
  Resolver.mavenLocal
)

scalacOptions := Seq(
  "-unchecked",
  "-deprecation",
  "-feature"
)

mainClass in (Compile, run) := Some("ConogramApplication")
mainClass in (Compile, packageBin) := Some("ConogramApplication")

unmanagedResourceDirectories in Compile += { baseDirectory.value / "src/main/prolog" }
