|  Product Backlog Item   | Sprint Task  | Volunteer   | Initial Estimate of Effort   |
|:-----------------------:|--------------|:-----------:|:----------------------------:|
| Implementation of game.singlePlayer.model and game logic. |  |  | 9 |
|  | Creating the game.singlePlayer.model for the data type "Grid" made-up by game board plus clues | Francesco Vignola | 3 |
|  | Definition of a Factory for creating grids | Francesco Vignola | 3 |
|  | Implementation of game logic into "Game" type and definition of interface needed by controller | Enrico Salvucci | 3 |
| Implementation of a random gaming grid generation system. |  |  | 6 |
|  | Investigate the most appropriate way to achieve table and clues generation via the Prolog engine (And check if it can be convenient) | Michael Bosello  | 4 |
|  | Implement grid generation according to the strategy found in the previous phase | Michael Bosello | 2 |
| GUI side representation of the game board. |  |  | 3 |
|  | Evaluate and implement the most appropriate method to display the game grid when updated data is provided | Silvia Gasparroni | 2 |
|  | As a logic of the puzzle, the system provides clues on the game screen to suggest the cell to be selected or not | Silvia Gasparroni | 1 |
| Integration of grid game.singlePlayer.model and GUI. |  | Silvia Gasparroni | 3 |
| As a player, I want to select a cell that I believe is full. |  |  | 5 |
| | Implement selection action in game.singlePlayer.model | Enrico Salvucci | 1 |
| | Implement selection action in controller | Silvia Gasparroni | 1 |
| | Implement selection action in GUI | Enrico Salvucci | 3 |