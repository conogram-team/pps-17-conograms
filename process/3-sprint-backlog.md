|  Product Backlog Item   | Sprint Task  | Volunteer   | Initial Estimate of Effort   |
|:-----------------------:|--------------|:-----------:|:----------------------------:|
| ** | Controller (MatchController) implementation of functions to be provided to the view and which will call the model (MatchModel) | Silvia Gasparroni | 1 |
| ** | Implement registration service | Enrico Salvucci + Francesco Vignola | +1 |
| ** | Implement login service | Enrico Salvucci + Francesco Vignola | +2 |
| ** | As a user, I would like to singin and login using the credentials chosen during registration. My data must be saved on the server so that other players can find me. | Enrico Salvucci + Francesco Vignola | +1 |
| ** | In controller module, Manage interaction with the REST-full service, this will be used in the login and registration controller (based on token as point 11) | Michael Bosello | +1 |
| 11. Implement a service, on the server side, to find another player who I can challenge. |  |  | 4 |
|  | Implement a service to get the ip of a user given his/her username | Enrico Salvucci + Francesco Vignola |2|
|  | Implement a service to get the ip of a user whose status is “online” in a random way | Enrico Salvucci + Francesco Vignola |2|
| 12. As a player I would like to choose either the single player or the multiplayer mode. |  |  | 5 |
|  | Update the GUI to let the user choose the single/multiplayer mode. The user, in such way, can also choose to challenge a specific user or get he/she randomly. | Michael Bosello |2|
|  | Implement the controller which links the new GUI and the previous controller/model and the controllers implemented in the task 13 | Michael Bosello |3|
| 13. As a user I would like to challenge someone other user searching it by username or by letting choose him/her by the system. |  |  | 6 |
|  | Implement the controller to use the REST service to get the ip of a user given his/her username or get him/her randomly | Silvia Gasparroni | 3 |
|  | Implemnt, view and controller, to manage a match: Notify network errors or an offline user, for example. Notify whether the other player took the challenge or not. In this first option show the match grid and in the other stay in the main menu. | Silvia Gasparroni | 3 |

** => Task from the previous sprint wich had not been finished