# Sprint retrospective - #1

* Tasks drawed up unclearly. Different team members had doubts about what they should do.
In the next sprints, more attention will be paid to define clear tasks.
* In the first sprint, several tasks depended on the work of others in order to complete.
As a possible solution, we propose the preliminary drafting of interfaces and their push so that everyone's work can proceed.
Moreover, a list on Trello will be dedicated to blocked tasks, in this way the members will know when others are stuck because of their tasks.