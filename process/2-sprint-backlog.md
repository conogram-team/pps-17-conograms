|  Product Backlog Item   | Sprint Task  | Volunteer   | Initial Estimate of Effort   |
|:-----------------------:|--------------|:-----------:|:----------------------------:|
| As a player, I would like to choose the level of difficulty. The density of the full cells depends on the size of the game table and the difficulty (as the difficulty increases, the density decreases). |  |  | 3 |
|  | Implement view that allows the user to select the difficulty level and the size of the grid before playing a game | Francesco Vignola | 2 |
|  | Controller (MatchController) implementation of functions to be provided to the view and which will call the model (MatchModel) | Enrico Salvucci | 1 |
| Generation of the game table based on the difficulty parameters chosen by the player. |  |  | 2 |
|  | Compute the number of full cells according to the difficulty chosen by the user: Low (70%), Medium (50%), High (30%) | Michael Bosello  | 1 |
|  | Implement the enum related to the difficulty: Low, Medium, High | Michael Bosello | 1 |
| As a player, I would like to have a maximum number of "lives". Once the lives are over, the game is over. If I select a cell that I consider full but in reality it is empty, I lose a life. |  |  | 4 |
|  | Implement Player data type | Silvia Gasparroni | 1 |
|  | Implement loss lives logic (MatchModel) | Silvia Gasparroni | 1 |
|  | View side implementation of methods to update the lives and to end the game in case lives are over | Silvia Gasparroni | 2 |
| As a user, I would like to singin and login using the credentials chosen during registration. My data must be saved on the server so that other players can find me. |  |  | 8 |
|  | Create login form | Michael Bosello | 1 |
|  | Create registration form | Michael Bosello | 1 |
|  | Implement User data type and add user reference in Player (Client side) | Michael Bosello | 1 |
|  | Implement login controller | Michael Bosello | 1 |
|  | Implement registration controller | Michael Bosello | 1 |
|  | In controller module, Manage interaction with the REST-full service, this will be used in the login and registration controller (based on token as point 11) | Michael Bosello | 3 |
| Implement, server side, a service by which the user can register and login. |  |  | 11 |
|  | Implement registration service | ??? | 2 |
|  | Implement login service | ??? | 1 |
|  | Implement user data type and Hibernate relations | ??? | 1 | (Server side)
|  | Take scalability measures | ??? | 5 |
|  | Login service and authenticated request are based on token calculated by username and password hash | ??? | 2 |