# Sprint retrospective - #2

* The issues shown in the first sprint review had been solved:
	- Writing the second sprint backlog the tasks were defined clearly.
	- The dependences among tasks were pointed out through the card "Blocked" in Trello in which now we move all tasks that depends to others.
* An error made designing the second task has been to underestimate tasks related to login and registration (which required more effort than the points that were assigned to).
