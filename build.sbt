name := "PPS-17-conograms"

version := "0.1"

lazy val root = project.in(file("."))
  .aggregate(client, server)

lazy val commonSettings = Seq(
  publishArtifact in (Compile, packageDoc) := false,
  autoAPIMappings := true
)

lazy val client = project.in(file("client"))
  .settings(commonSettings: _*)

lazy val server = project.in(file("server"))
  .settings(commonSettings: _*)


