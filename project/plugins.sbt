addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.6")

addSbtPlugin("com.lightbend.lagom" % "lagom-sbt-plugin" % "1.4.0")

addSbtPlugin("com.lightbend.rp" % "sbt-reactive-app" % "1.5.0")
