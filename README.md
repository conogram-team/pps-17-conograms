# Conograms

The idea of the project is to produce a distributed and multiplayer version of the Japanese Nonogram puzzle. The original contribution of the group lies in the invention of a competitive multiplayer version of the game where two users challenge each other.


One can find the report of the project in the Downloads section.

## Quick start
The application is composed by a client and a server.


### To run the client

`java -jar conogram-client.jar`

### To run the server

Execute runAll within the root project

`sbt runAll`

#### Import in intellij
> import project

> import project from external model

> sbt

#### You can directly run both within sbt:
+ To run the client

From the root project move to the client project 

    sbt project client 

 
and then execute run

    sbt run
    
+ To run the server

Execute runAll within root project or server project

    sbt runAll
     
