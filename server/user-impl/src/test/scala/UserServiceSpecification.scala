import akka.Done
import akka.actor.ActorSystem
import com.lightbend.lagom.scaladsl.api.transport.BadRequest
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraSession
import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
import com.lightbend.lagom.scaladsl.testkit.ServiceTest
import org.scalatest._
import persistence.{Repository, UsersSpecification}
import user.api.UserApi
import user.model.User

import scala.concurrent.ExecutionContext


class UserServiceSpecification extends FeatureSpec with GivenWhenThen with BeforeAndAfter {

  lazy val server: ServiceTest.TestServer[UserApplication with LocalServiceLocator] =
    ServiceTest.startServer(
      ServiceTest.defaultSetup.withCassandra()) { context => new UserApplication(context) with LocalServiceLocator }

  implicit val system: ActorSystem = server.application.actorSystem
  implicit val jsonSerializerRegistry: UserSerializerRegistry.type = server.application.jsonSerializerRegistry
  implicit val cassandraSession: CassandraSession = server.application.cassandraSession
  implicit val userRepository: Repository[User] = server.application.userRepository
  implicit val executionContext: ExecutionContext = ExecutionContext.global

  lazy val client: UserApi = server.serviceClient.implement[UserApi]

  val username = "alice"
  val passwordHash = Some("$2a$04$vk/wi61uVgpualJjvS72XOmhyTAIJ1g2mEP19dAi59fGkNNhheide")
  val ip = Some("192.168.1.12")


  after {
    userRepository.read(new UsersSpecification)
      .map(users => users.foreach(user => client.logoutUser().invoke(user)))
  }

  info("As a conogram consumer I would like to create a new user")

  feature("Create a new user") {
    scenario("User inserts an empty username and an empty password") {
      Given("a username and a password hash (provided by the client)")
      val username = ""
      val passwordHash = ""

      Then("create a new user and store it into the database")
      client.createUser()
        .invoke(User(username, Some(passwordHash), ip)).map(r => assertThrows[BadRequest](r))
    }

    scenario("Client forwards undefined username and password to the server") {
      Given("a username and a password hash (provided by the client)")
      val username = null
      val passwordHash = null

      Then("create a new user and store it into the database")
      client.createUser()
        .invoke(User(username, Some(passwordHash), ip)).map(r => assertThrows[BadRequest](r))
    }

    scenario("User inserts a username and a password whose are not present in the db") {
      Given("a username and a password hash (provided by the client)")
      Then("create a new user and store it into the database")
      client.createUser()
        .invoke(User(username, passwordHash, ip)).map(r => assert(r == Done))
    }

    scenario("User inserts a username and a password hash still present in the db") {
      Given("a username and a password hash whose are still present in the db")

      Then("create a new user and store it into the database")
      val createFuture = for {
        alreadyRegisteredUser <- client.createUser()
          .invoke(User(username, passwordHash, ip))
      } yield assertThrows[BadRequest](alreadyRegisteredUser)
    }

  }

  info("As a registered user I would like to login into the system")

  feature("Login to get the authentication token") {
    scenario("User inserts valid username and password") {
      Given("a username and a password hash (provided by the client, during signup)")

      Then("Two tokens of the same user must be equals")
      val loginFuture = for {
        performLogin <- client.loginUser().invoke(User(username, passwordHash, ip))
      } yield assert(performLogin == Done)
    }

    scenario("User does not insert valid password") {
      Given("a registered user and a wrong password hash")
      val wrongPasswordHash = "$2a$04$vk/wi61uVgpualJjvS72XOmhyTAIJ1g2mEP19dwrongwrongwrong"

      Then("an exception is thrown")
      val loginFuture = for {
        user <- client.createUser()
          .invoke(User(username, passwordHash, ip))
        loginWithWrongPassword <- client.loginUser().invoke(User(username, passwordHash, ip))
      } yield assertThrows[BadRequest](loginWithWrongPassword)

    }

    scenario("User does not insert valid username") {
      Given("a registered user and a wrong username")
      val wrongUsername = "aliceWrong"

      Then("an exception is thrown")
      val loginFuture = for {
        userWithWrongUsername <- client.loginUser().invoke(User(wrongUsername, passwordHash, ip))
      } yield assertThrows[BadRequest]()

    }

    scenario("User inserts a non defined username") {
      Given("a null username")
      val nullUsername = null

      Then("an exception is thrown")

      val nullUsernameFuture = for {
        user <- client.createUser()
          .invoke(User(nullUsername, passwordHash, ip))
        userWithWrongUsername <- client.loginUser().invoke(User(nullUsername, passwordHash, ip))
      } yield assertThrows[BadRequest]()

    }

    scenario("User inserts a non defined password") {
      Given("a null username")
      val nullPassword = null

      Then("an exception is thrown")

      val nullPasswordFuture = for {
        user <- client.createUser()
          .invoke(User(username, nullPassword, ip))
        userWithWrongUsername <- client.loginUser().invoke(User(username, nullPassword, ip))
      } yield assertThrows[BadRequest]()

    }
  }

  info("As a developer I would like to retrieve an user ip from his/her username")

  feature("Get ip of an user from his/her username") {

    scenario("The client requests ip of an online") {
      Given("a username of a online user")
      Then("the retrieved ip is equal to known ip")
      client.getIpByUsername(username).invoke().map(retrievedIp => assert(retrievedIp == ip.get))
    }

    scenario("The client requests ip of an non-existent user") {
      Given("a username of an user who has never registered before")
      val bobUsername = "bob"

      Then("you receive a BadRequest")
      client.getIpByUsername(bobUsername).invoke().map(retrievedIp => assertThrows[BadRequest](retrievedIp))
    }

    scenario("The client requests ip of an offline user") {
      Given("a username of an user who is offline")
      val bobUsername = "bob"

      Then("throw not found exception")
      val nullPasswordFuture = for {
        user <- client.createUser()
          .invoke(User(bobUsername, passwordHash, ip))
        ip <- client.getIpByUsername(bobUsername).invoke()
      } yield assertThrows[BadRequest](ip)
    }

  }

  info("As a conogram consumer I would like to get a random online user to challenge")

  feature("Get the online user") {

    scenario("User wants a random user when there are some users online") {
      Given("a logged in user and a group of online users")
      val alice = User(username, passwordHash, ip)
      val bob = User("bob", Some("$4a$04$vk/wi61uVgpualJjvS72XOmhyTAfjfjfkkfirir8393jd"), Some("192.168.0.2"))
      val carl = User("carl", Some("$6a$04$vk/wi61uVgpualJjvS72XOmhnjfrijrei8438438934jf"), Some("192.168.0.3"))
      val mike = User("mike", Some("$8a$04$vk/wi61uVgpualJjvS72XOmhnjfr85849490404004040"), Some("127.0.0.1"))

      When("the user requests to have a random online user (different from itself)")
      Then("the user obtained must have a valid IP")
      for {
        registeredBob <- client.createUser().invoke(bob)
        registeredCarl <- client.createUser().invoke(carl)
        loggedAlice <- client.loginUser().invoke(alice)
        loggedBob <- client.loginUser().invoke(bob)
        loggedCarl <- client.loginUser().invoke(carl)
        user <- client.getRandomUser(mike.username).invoke()
      } yield assert(user.ip.isDefined && !user.ip.get.isEmpty)

    }

    scenario("User wants a random user when no user is online") {
      Given("a logged in user and only it")
      val alice = User(username, passwordHash, ip)

      When("the user requests to have a random online user (different from itself)")
      Then("the user does not get anything")
      for {
        loggedAlice <- client.loginUser().invoke(alice)
        random <- client.getRandomUser(alice.username).invoke()
      } yield assertThrows[BadRequest](random)
    }

  }

}