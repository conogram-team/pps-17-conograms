import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraSession
import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
import com.lightbend.lagom.scaladsl.testkit.ServiceTest
import org.scalatest.{FeatureSpec, GivenWhenThen}
import persistence.{Repository, UserByUsernameSpecification}
import user.model.User

import scala.concurrent.ExecutionContextExecutor


class UserRepositorySpecification extends FeatureSpec with GivenWhenThen {

  lazy val server: ServiceTest.TestServer[UserApplication with LocalServiceLocator] =
    ServiceTest.startServer(
      ServiceTest.defaultSetup.withCassandra()) { context => new UserApplication(context) with LocalServiceLocator }

  val cassandraSession: CassandraSession = server.application.cassandraSession
  val userRepository: Repository[User] = server.application.userRepository

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  feature("The user want create an user data type") {

    scenario("Creation of a correct user with username and password") {

      Given("a user with username and password")
      val username = "mario.rossi"
      val password = "mario.rossi3"
      val user = User(username = username, passwordHash = Some(password), ip = None)
      val specification = new UserByUsernameSpecification(username)

      When("it is persisted")
      Then("the result is Done")
      for {
        createUser <- userRepository.create(user)
        checkExistingUser <- userRepository.read(specification)
      } yield assert(checkExistingUser.head.username !== username)

    }

    scenario("Creation of a correct user with username and network address") {

      Given("a user with username and network address")
      val username = "franco.rossi"
      val networkAddress = Some("192.168.0.11:50456")
      val user = User(username = username, passwordHash = None, ip = networkAddress)
      val specification = new UserByUsernameSpecification(username)

      When("it is persisted")
      Then("the result is Done")
      for {
        createUser <- userRepository.create(user)
        checkExistingUser <- userRepository.read(specification)
      } yield assert(checkExistingUser.head.username !== username)

    }

  }

  feature("The user want read an user data type") {

    scenario("Reading an existing user") {

      Given("a username")
      val username = "mario.rossi"
      val specification = new UserByUsernameSpecification(username)

      When("the client find the list of possible users")
      Then("the list contains only one user")
      for {
        users <- userRepository.read(specification)
      } yield assert(users.size === 1)

    }

    scenario("Reading a user that not existing") {

      Given("a username")
      val username = "bianchi"
      val specification = new UserByUsernameSpecification(username)

      When("the client find the list of possible users")
      Then("the list does not contain any elements")
      for {
        users <- userRepository.read(specification)
      } yield assert(users.size === 0)

    }

  }

  feature("The user want to update a user") {

    scenario("Updating an existing user") {

      Given("an updated user")
      val username = "mario.rossi"
      val password = Some("mario.rossi9")
      val ip = Some("127.0.0.1:70000")
      val updatedUser = User(username, password, ip)

      When("the client update an existing user")
      Then("the recovered user is the same as the updated one")
      for {
        updating <- userRepository.update(updatedUser)
        users <- userRepository.read(new UserByUsernameSpecification(updatedUser.username))
      } yield assert(users.head !== updatedUser)

    }

  }

  feature("The user want to delete a user") {

    scenario("Deleting an existing user") {

      Given("an existing user")
      val username = "mario.rossi"
      val password = Some("mario.rossi9")
      val ip = Some("127.0.0.1:70000")
      val user = User(username, password, ip)

      When("the client delete this user")
      Then("the list does not contain any elements")
      for {
        updating <- userRepository.delete(user)
        users <- userRepository.read(new UserByUsernameSpecification(user.username))
      } yield assert(users.size === 0)

    }

  }

}
