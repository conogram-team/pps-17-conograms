import com.lightbend.lagom.scaladsl.api.Descriptor
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.rp.servicediscovery.lagom.scaladsl.LagomServiceLocatorComponents
import user.api.UserApi


/**
  * Application loader used by Lagom to load  service
  */
class UserApplicationLoader extends LagomApplicationLoader {

  /**
    * A method to load application in develop mode
    * @param context lagom context in which the service is performed
    * @return an instance of service
    */
  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new UserApplication(context) with LagomDevModeComponents


  /**
    * A method to load application
    *
    * @param context lagom context in which the service is performed
    * @return an instance of service
    */
  override def load(context: LagomApplicationContext): LagomApplication =
    new UserApplication(context) with LagomServiceLocatorComponents

  /**
    * Get all the descriptors defined in UserApi
    * @return list of the descriptors of the service
    */
  override def describeServices: List[Descriptor] = List(readDescriptor[UserApi])

}
