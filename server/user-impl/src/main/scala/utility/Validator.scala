package utility

import user.model.User

/**
  * Helper to validate:
  *   - one or multiple string
  *   - a user
  *   - a player
  */
object Validator {

  /**
    * A method for validation of a string
    *
    * @param argument string to validate
    * @return true if valid, false if not valid
    */
  def validate(argument: String): Boolean =
    argument.trim.nonEmpty && argument != null


  /**
    * A method for validation of one or more strings
    *
    * @param arguments one or more strings to validate
    * @return true if all strings are valid, false if are not valid
    */
  def validate(arguments: String*): Boolean =
    arguments.forall(argument => this.validate(argument))


  /**
    * A method for validation of a user
    *
    * @param user to validate
    * @return true if user is valid, false if not valid
    */
  def validateUser(user: User): Boolean =
    this.validate(user.username) && this.validate(user.passwordHash.getOrElse(""))


  /**
    * A method for validation of a player
    *
    * @param player an online user to validate
    * @return true if player is valid, false if not valid
    */
  def validatePlayer(player: User): Boolean =
    this.validate(player.username) && validate(player.ip.getOrElse(""))

}
