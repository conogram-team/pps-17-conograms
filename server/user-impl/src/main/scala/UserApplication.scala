import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server._
import com.softwaremill.macwire._
import persistence.{Repository, UserRepository}
import play.api.libs.ws.ahc.AhcWSComponents
import user.UserService
import user.api.UserApi
import user.model.User

import scala.collection.immutable.Seq


/**
  * Class that sets the components useful for the service
  * @param context lagom context in which the service is performed
  */
abstract class UserApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with CassandraPersistenceComponents
    with AhcWSComponents {


  /**
    * Bind the service that this server provides
    */
  override lazy val lagomServer: LagomServer = serverFor[UserApi](wire[UserService])


  /**
    * Register the JSON serializer registry
    */
  override lazy val jsonSerializerRegistry: UserSerializerRegistry.type = UserSerializerRegistry


  /**
    * Register dependencies
    */
  lazy val userRepository: Repository[User] = wire[UserRepository]

}


/**
  * Serializer used by Lagom to send User object via network
  */
object UserSerializerRegistry extends JsonSerializerRegistry {

  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[User]
  )

}
