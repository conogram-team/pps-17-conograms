package persistence

import akka.Done

import scala.concurrent.Future


/**
  * Trait useful for specifying search criteria
  */
trait Specification {

  /**
    * Generate a string with the
    * appropriate search criteria
    *
    * @return a string with search criteria
    */
  def query: String

}


/**
  * Trait that provides a contract to perform the
  * CRUD operation on objects of a certain type
  *
  * @tparam T data type on which the CRUD operations
  *           are performed
  */
trait Repository[T] {

  /**
    *
    * @param item element to add into warehouse
    * @return returns a result that has not yet been determined
    */
  def create(item: T): Future[Done]

  /**
    *
    * @param specification represents the search criteria
    * @return returns a result that has not yet been determined
    */
  def read(specification: Specification): Future[Traversable[T]]

  /**
    *
    * @param item element of the warehouse to update
    * @return returns a result that has not yet been determined
    */
  def update(item: T): Future[Done]

  /**
    *
    * @param item element to be removed from the warehouse
    * @return returns a result that has not yet been determined
    */
  def delete(item: T): Future[Done]

  /**
    *
    * @param items all elements to be removed from the warehouse
    * @return returns a result that has not yet been determined
    */
  def deleteAll(items: Traversable[T]): Future[Done]

}
