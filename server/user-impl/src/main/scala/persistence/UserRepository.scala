package persistence

import akka.Done
import com.datastax.driver.core.Row
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraSession
import user.model.User

import scala.concurrent.{ExecutionContext, Future}

/**
  * @see [[Specification]]
  */
class UsersSpecification extends Specification {

  private val SelectAllStatement = "SELECT * FROM user"

  /**
    * @see [[Specification]]
    * @return a string with search criteria
    */
  override def query: String = SelectAllStatement

}


/**
  * @see [[Specification]]
  * @param username username of the user to search
  */
class UserByUsernameSpecification(username: String) extends Specification {

  private val SelectStatement = "SELECT * FROM user WHERE username = '%s'"

  /**
    * @see [[Specification]]
    * @return a string with search criteria
    */
  override def query: String = SelectStatement.format(username)

}


/**
  * @see [[Repository]]
  * @param cassandraSession database session on which to perform writes and readings
  * @param executionContext on which to perform database operations safely, without concurrency issues
  */
class UserRepository(cassandraSession: CassandraSession)(implicit executionContext: ExecutionContext) extends Repository[User] {
  private val CreateUserTableStatement = "CREATE TABLE IF NOT EXISTS user_service.user(username text PRIMARY KEY, passwordHash text, ip text)"
  private val InsertUserStatement = "INSERT INTO user (username, passwordHash, ip) VALUES ('%s', '%s', '%s') IF NOT EXISTS"
  private val DeleteUserStatement = "DELETE FROM user WHERE username = '%s'"
  private val UpdateUserStatement = "UPDATE user SET ip = '%s' where username = '%s'"
  cassandraSession.executeCreateTable(CreateUserTableStatement)


  /**
    * @see [[Repository]]
    * @param user user to add into warehouse
    * @return returns a result that has not yet been determined
    */
  override def create(user: User): Future[Done] = {
    val statement = InsertUserStatement
      .format(user.username, user.passwordHash.getOrElse(""), user.ip.getOrElse(""))
    cassandraSession.executeWrite(statement)
  }


  /**
    * @see [[Repository]]
    * @param specification represents the search criteria
    * @return returns a result that has not yet been determined
    */
  override def read(specification: Specification): Future[Traversable[User]] =
    for (one <- cassandraSession.selectAll(specification.query))
      yield one.map(row => mapRowToUser(row))


  /**
    * @see [[Repository]]
    * @param user user of the warehouse to update
    * @return returns a result that has not yet been determined
    */
  override def update(user: User): Future[Done] = {
    val statement = UpdateUserStatement.format(user.ip.getOrElse(""), user.username)
    cassandraSession.executeWrite(statement)
  }


  /**
    * @see [[Repository]]
    * @param user user to be removed from the warehouse
    * @return returns a result that has not yet been determined
    */
  override def delete(user: User): Future[Done] = {
    val statement = DeleteUserStatement.format(user.username)
    cassandraSession.executeWrite(statement)
  }


  /**
    * @see [[Repository]]
    * @param users all users to be removed from the warehouse
    * @return returns a result that has not yet been determined
    */
  override def deleteAll(users: Traversable[User]): Future[Done] = {
    val result = for (item <- users) yield this.delete(item)
    result.head
  }


  private def mapRowToUser(row: Row): User = {
    val username = row.getString("username")
    val passwordHash = row.getString("passwordHash") match {
      case "" => None
      case passwordHashed => Some(passwordHashed)
    }
    val ip = row.getString("ip") match {
      case "" => None
      case ipAddress => Some(ipAddress)
    }

    User(username, passwordHash, ip)
  }

}
