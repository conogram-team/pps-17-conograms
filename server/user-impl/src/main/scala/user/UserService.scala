package user

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.transport.{BadRequest, ResponseHeader}
import com.lightbend.lagom.scaladsl.server.ServerServiceCall
import persistence._
import user.api.{JwtToken, UserApi}
import user.model.User
import utility.Validator._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random


/**
  * Service that provides all user-related operations useful for the game
  *
  * @param userRepository a repository use to make persistent one or more users
  * @param executionContext context used by lagom to run the service calls
  */
class UserService(userRepository: Repository[User])(implicit executionContext: ExecutionContext) extends UserApi {

  private val IllegalArgumentMessage = "All required data should not be empty or non defined"
  private val UserExistMessage = "User already exist"
  private val UserNotFoundMessage = "The user has not been found"
  private val UserNotOnlineMessage = "The user is not online"


  /**
    * Creating a new user
    * The creation of a new user requires a user object with mandatory username, password hashed and ip both optional
    *
    * @return String Body Parameter  User to create
    */
  override def createUser(): ServiceCall[User, Done] = ServiceCall { user =>

    def task(users: List[User]): Future[Done] = users match {
      case _ :: _ => Future.failed(BadRequest(UserExistMessage))
      case _ => userRepository.create(user)
    }

    ifValidRequestThen(user, validateUser,
      readUsers(new UserByUsernameSpecification(user.username), task))

  }


  /**
    * Get a user with a different username from the one supplied
    * Get a random user who has logged into the system and who has a different user ame from the one supplied
    *
    * @param usernameToExclude Username that does not have to match.
    * @return User
    */
  override def getRandomUser(usernameToExclude: String): ServiceCall[NotUsed, User] = ServiceCall { _ =>

    def filterOnlineUser(users: Traversable[User]): Traversable[User] =
      users.filter(user => user.ip.isDefined)
        .filter(user => user.ip.nonEmpty)
        .filter(user => user.username != usernameToExclude)

    def task(users: List[User]): Future[User] = filterOnlineUser(users) match {
      case filteredUsers@_ :: _ =>
        val randomUser: User = filteredUsers(Random.nextInt(filteredUsers.size))
        Future.successful(User(randomUser.username, None, randomUser.ip))
      case _ =>
        Future.failed(BadRequest(UserNotFoundMessage))
    }

    ifValidRequestThen(usernameToExclude, validate,
      readUsers(new UsersSpecification, task))

  }


  /**
    * Get the network address of a logged in user
    * Get the network address of a logged in user who has the same username as the one supplied
    *
    * @param username The username of the user whose IP is wanted.
    * @return String
    */
  override def getIpByUsername(username: String): ServiceCall[NotUsed, String] = ServiceCall { _ =>

    def task(users: List[User]): Future[String] = users match {
      case head :: _ if head.ip.isDefined && head.ip.get.nonEmpty =>
        Future.successful(head.ip.get)
      case Nil => Future.failed(BadRequest(UserNotFoundMessage))
      case _ => Future.failed(BadRequest(UserNotOnlineMessage))
    }

    ifValidRequestThen(username, validate,
      readUsers(new UserByUsernameSpecification(username), task))

  }


  /**
    * User login into the system
    * A new session is started for the user
    *
    * @return String Body Parameter  User who log in
    */
  override def loginUser(): ServiceCall[User, Done] = ServerServiceCall { (request, user) =>

    val AuthorizationHeaderName = "Authorization"
    val WrongCredentialsMessage = "Wrong credentials"

    def tokenMatches(username: String, storedPasswordHash: String, generateToken: (String, String) => String) =
      generateToken(username, storedPasswordHash) == request.getHeader(AuthorizationHeaderName).getOrElse("")

    def task(users: List[User]): Future[(ResponseHeader, Done)] = users match {
      case head :: _ if tokenMatches(user.username, head.passwordHash.get, JwtToken.generateToken) =>
        userRepository.update(User(user.username, user.passwordHash, user.ip))
        Future.successful((ResponseHeader.Ok, Done))
      case Nil => Future.failed(BadRequest(UserNotFoundMessage))
      case _ => Future.failed(BadRequest(WrongCredentialsMessage))
    }

    ifValidRequestThen(user, validatePlayer,
      readUsers(new UserByUsernameSpecification(user.username), task))
  }


  /**
    * User logout from the system
    * The session of the currently logged-in user ends
    *
    * @return void Body Parameter  User who log out
    */
  override def logoutUser(): ServiceCall[User, Done] = ServiceCall { user =>

    def task(users: List[User]): Future[Done] = users match {
      case head :: _ if head.ip.isDefined && head.ip.get.nonEmpty =>
        val logoutUser = User(user.username, user.passwordHash, None)
        userRepository.update(logoutUser)
      case Nil => Future.failed(BadRequest(UserNotFoundMessage))
      case _ => Future.failed(BadRequest(UserNotOnlineMessage))
    }

    ifValidRequestThen(user.username, validate,
      readUsers(new UserByUsernameSpecification(user.username), task))

  }


  /**
    * Updating user data
    * Updating of user data: username, password hashed and / or network address
    *
    * @param username Username of the user to be updated
    * @return void Body Parameter  User to be updated
    */
  override def updateUser(username: String): ServiceCall[User, Done] = ServiceCall { user =>

    def task(users: List[User]): Future[Done] = users match {
      case _ :: _ => userRepository.update(user)
      case _ => Future.failed(BadRequest(UserExistMessage))
    }

    ifValidRequestThen(username, validate,
      readUsers(new UserByUsernameSpecification(username), task))

  }


  private def ifValidRequestThen[A, B](toValidate: A, validator: A => Boolean, futureTask: Future[B]): Future[B] = {
    if (validator(toValidate)) {
      futureTask
    } else {
      Future.failed(BadRequest(IllegalArgumentMessage))
    }
  }


  private def readUsers[T](specification: Specification, task: List[User] => Future[T]): Future[T] =
    userRepository
      .read(specification)
      .flatMap(users => task(users.toList))

}
