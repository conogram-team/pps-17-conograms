package user.model

import play.api.libs.json._

/**
  * Models the user signed on the system.
  *
  * @param username     the username of the user.
  * @param passwordHash the hashed password of the user.
  * @param ip           the ip of the user client.
  */
case class User(
                 username: String,
                 passwordHash: Option[String],
                 ip: Option[String]
               )

/**
  * Companion object for an user signed on the system.
  */
object User {

  /**
    * Let the user serializable as json object.
    */
  implicit val format: Format[User] = Json.format
}