package user.api

import com.typesafe.config.ConfigFactory
import pdi.jwt.{Jwt, JwtAlgorithm, JwtClaim, JwtJson}
import play.api.libs.json.{Format, Json}

/**
  * Trait which define the strategy to generate a token.
  */
sealed trait Token {

  /**
    * Generate a token according to the strategy adopted in the trait implementation.
    * @param username the username of the user that is performing login.
    * @return
    */
  def generateToken(username: String, passwordHash: String): String
}

/**
  * A Singleton object representing a token according to the Jason Web Token open standard.
  */
object JwtToken extends Token {

  val JwtConfigurationBasename = "jwt.conf"
  val JwtTokenExpirationPath = "jwt.token.auth.expirationInSeconds"

  /**
    * The expiration time of the JWT token.
    */
  val authExpiration: Int = ConfigFactory
                              .load(JwtConfigurationBasename)
                              .getInt(JwtTokenExpirationPath)
  /**
    * The hashing algoritm used to compute the signature.
    */
  val algorithm: JwtAlgorithm.HS256.type = JwtAlgorithm.HS256

  def generateToken(username: String, passwordHash: String): String = {
    val PayloadValue = s"""{"user":$username}"""
    Jwt.encode(PayloadValue, passwordHash, algorithm)
  }
}